/****************************************************************************
 * arch/arm/src/stm32f7/stm32_hptc.c
 *
 *   Copyright (C) 2015 Wail Khemir. All rights reserved.
 *   Copyright (C) 2015 Omni Hoverboards Inc. All rights reserved.
 *   Authors: Wail Khemir <khemirwail@gmail.com>
 *            Paul Alexander Patience <paul-a.patience@polymtl.ca>
 *            Stefan Nowak
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include "clock/clock.h" //systimer USEC_PER_TICK

#include "nvic.h"  //NVIC register NVIC_SYSTICK_CTRL

#include <nuttx/irq.h>
#include <semaphore.h>
#include <arch/board/board.h>

#include "up_internal.h"
#include "ram_vectors.h"
#include "up_arch.h"

#include "chip.h"
#include "stm32_hptc.h"


/* This module then only compiles if there is at least one enabled timer
 * intended for use with the HPTC upper half driver.
 */
//from stm32_tim_lower

//#include <sys/types.h>

#include <string.h>

#include <nuttx/timers/hptc.h>

//#include "stm32_tim.h"


#if 0  //from stm32_tim.c
#include <nuttx/config.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>

#include <errno.h>
#include <debug.h>

#include <arch/board/board.h>

#include "chip.h"
#include "up_internal.h"
#include "up_arch.h"

#include "stm32.h"
#include "stm32_gpio.h"
#include "stm32_hptc.h"

#endif


#define HPTC_TRIGGER_SYSTICK



#if defined(CONFIG_HPTC) && \
    (defined(CONFIG_STM32F7_HPTC_TICKTIMER_TIM1) || \
	 defined(CONFIG_STM32F7_HPTC_TICKTIMER_TIM2) || \
	 defined(CONFIG_STM32F7_HPTC_TICKTIMER_TIM5) )

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define STM32F7_TIM1_HPTC_RES   16
#define STM32F7_TIM2_HPTC_RES   32
#define STM32F7_TIM3_HPTC_RES   16
#define STM32F7_TIM4_HPTC_RES   16
#define STM32F7_TIM5_HPTC_RES   32
#define STM32F7_TIM8_HPTC_RES   16
#define STM32F7_TIM9_HPTC_RES   16
#define STM32F7_TIM10_HPTC_RES  16
#define STM32F7_TIM11_HPTC_RES  16
#define STM32F7_TIM12_HPTC_RES  16
#define STM32F7_TIM13_HPTC_RES  16
#define STM32F7_TIM14_HPTC_RES  16



#ifdef CONFIG_STM32F7_HPTC_TICKTIMER_TIM1

//possible initial sync master for TIM1
//	"ITR0: TIM5_TRGO"
//	"ITR1: TIM2_TRGO"
//	"ITR2: TIM3_TRGO"
//	"ITR3: TIM4_TRGO"
#   define HPTC_TICKTIMER_ISSLAVE  0
#   define HPTC_TICKTIMER_TYPE 1
#   define HPTC_TICKTIMER_OC_MASK 0x1E00

#   define HPTC_TICK_IRQ          STM32_IRQ_TIM1
#   define HPTC_TICKTIMER_BASE    STM32_TIM1_BASE
#   define HPTC_TICKTIMER_CNT     STM32_TIM1_CNT
#   define HPTC_TICKTIMER_SR      STM32_TIM1_SR
#   define HPTC_TICKTIMER_DIER    STM32_TIM1_DIER
#   define HPTC_TICKTIMER_CCMR1   STM32_TIM1_CCMR1
#   define HPTC_TICKTIMER_CCMR2   STM32_TIM1_CCMR2

#   define HPTC_TICKTIMER_RES     STM32F7_TIM1_HPTC_RES
#   define HPTC_TICKTIMER_CHANNELS  4

#if defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL1)
#   define HPTC_TICKTIMER_CC      STM32_TIM1_CCR1
#   define HPTC_TICKTIMER_SR_CCIF ATIM_SR_CC1IF
#   define HPTC_TICKTIMER_DIER_CCIE ATIM_DIER_CC1IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL2)
#   define HPTC_TICKTIMER_CC      STM32_TIM1_CCR2
#   define HPTC_TICKTIMER_SR_CCIF ATIM_SR_CC2IF
#   define HPTC_TICKTIMER_DIER_CCIE ATIM_DIER_CC2IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL3)
#   define HPTC_TICKTIMER_CC      STM32_TIM1_CCR3
#   define HPTC_TICKTIMER_SR_CCIF ATIM_SR_CC3IF
#   define HPTC_TICKTIMER_DIER_CCIE ATIM_DIER_CC3IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL4)
#   define HPTC_TICKTIMER_CCR      STM32_TIM2_CCR4
#   define HPTC_TICKTIMER_SR_CCIF GTIM_SR_CC4IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC4IE
#endif

#endif


#ifdef CONFIG_STM32F7_HPTC_TICKTIMER_TIM2

//possible initial sync master for TIM2
//	"ITR0: TIM1_TRGO"
//	"ITR1: TIM8_TRGO"
//	"ITR2: TIM3_TRGO"
//	"ITR3: TIM4_TRGO"
#   define HPTC_TICKTIMER_ISSLAVE  0
#   define HPTC_TICKTIMER_TYPE 2
#   define HPTC_TICKTIMER_OC_MASK 0x1E00


#   define HPTC_TICK_IRQ            STM32_IRQ_TIM2
#   define HPTC_TICKTIMER_BASE      STM32_TIM2_BASE
#   define HPTC_TICKTIMER_CNT       STM32_TIM2_CNT
#   define HPTC_TICKTIMER_SR        STM32_TIM2_SR
#   define HPTC_TICKTIMER_DIER      STM32_TIM2_DIER
#   define HPTC_TICKTIMER_CCMR1     STM32_TIM2_CCMR1
#   define HPTC_TICKTIMER_CCMR2     STM32_TIM2_CCMR2
#   define HPTC_TICKTIMER_RES       STM32F7_TIM2_HPTC_RES
#   define HPTC_TICKTIMER_CHANNELS  4


#if defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL1)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR1
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC1IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC1IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL2)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR2
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC2IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC2IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL3)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR3
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC3IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC3IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL4)
#   define HPTC_TICKTIMER_CCR       STM32_TIM2_CCR4
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC4IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC4IE

#endif

#endif


#ifdef CONFIG_STM32F7_HPTC_TICKTIMER_TIM5

//possible initial sync master for TIM5
//	"ITR0: TIM2_TRGO"
//	"ITR1: TIM3_TRGO"
//	"ITR2: TIM4_TRGO"
//	"ITR3: TIM5_TRGO"

// only combination TIM2+TIM5 implemented
#   ifdef CONFIG_STM32F7_HPTC_AUXTIMER_TIM2
#     define HPTC_TICKTIMER_ISSLAVE  1
#     define HPTC_TICKTIMER_TS GTIM_SMCR_ITR2
#   else
#     define HPTC_TICKTIMER_ISSLAVE  0
#   endif

#   define HPTC_TICKTIMER_TYPE 2
#   define HPTC_TICKTIMER_OC_MASK 0x1E00

#   define HPTC_TICK_IRQ            STM32_IRQ_TIM5
#   define HPTC_TICKTIMER_BASE      STM32_TIM5_BASE
#   define HPTC_TICKTIMER_CNT       STM32_TIM5_CNT
#   define HPTC_TICKTIMER_SR        STM32_TIM5_SR
#   define HPTC_TICKTIMER_DIER      STM32_TIM5_DIER
#   define HPTC_TICKTIMER_CCMR1     STM32_TIM5_CCMR1
#   define HPTC_TICKTIMER_CCMR2     STM32_TIM5_CCMR2
#   define HPTC_TICKTIMER_RES       STM32F7_TIM5_HPTC_RES
#   define HPTC_TICKTIMER_CHANNELS  4

#if defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL1)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR1
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC1IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC1IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL2)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR2
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC2IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC2IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL3)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR3
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC3IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC3IE

#elif defined(CONFIG_STM32F7_HPTC_TICKTIMER_TICKCHANNEL4)
#   define HPTC_TICKTIMER_CCR       STM32_TIM5_CCR4
#   define HPTC_TICKTIMER_SR_CCIF   GTIM_SR_CC4IF
#   define HPTC_TICKTIMER_DIER_CCIE GTIM_DIER_CC4IE

#endif

#endif



#ifdef CONFIG_STM32F7_HPTC_AUXTIMER_TIM1
#   define HPTC_AUXTIMER
#	define HPTC_AUXTIMER_ISSLAVE  0
#   define HPTC_AUXTIMER_TYPE 1
#   define HPTC_AUXTIMER_OC_MASK 0x1E00
#   define HPTC_AUX_IRQ          STM32_IRQ_TIM1
#   define HPTC_AUXTIMER_BASE    STM32_TIM1_BASE
#   define HPTC_AUXTIMER_CNT     STM32_TIM1_CNT
#   define HPTC_AUXTIMER_SR      STM32_TIM1_SR
#   define HPTC_AUXTIMER_DIER    STM32_TIM1_DIER
#   define HPTC_AUXTIMER_CCMR1   STM32_TIM1_CCMR1
#   define HPTC_AUXTIMER_CCMR2   STM32_TIM1_CCMR2
#   define HPTC_AUXTIMER_RES     STM32F7_TIM1_HPTC_RES
#   define HPTC_AUXTIMER_CHANNELS  4
#endif


#ifdef CONFIG_STM32F7_HPTC_AUXTIMER_TIM2
#   define HPTC_AUXTIMER
#	define HPTC_AUXTIMER_ISSLAVE  0
#   define HPTC_AUXTIMER_TYPE 2
#   define HPTC_AUXTIMER_OC_MASK 0x1E00
#   define HPTC_AUX_IRQ            STM32_IRQ_TIM2
#   define HPTC_AUXTIMER_BASE      STM32_TIM2_BASE
#   define HPTC_AUXTIMER_CNT       STM32_TIM2_CNT
#   define HPTC_AUXTIMER_SR        STM32_TIM2_SR
#   define HPTC_AUXTIMER_DIER      STM32_TIM2_DIER
#   define HPTC_AUXTIMER_CCMR1     STM32_TIM2_CCMR1
#   define HPTC_AUXTIMER_CCMR2     STM32_TIM2_CCMR2
#   define HPTC_AUXTIMER_RES       STM32F7_TIM2_HPTC_RES
#   define HPTC_AUXTIMER_CHANNELS  4
#endif


#ifdef CONFIG_STM32F7_HPTC_AUXTIMER_TIM5
#   define HPTC_AUXTIMER
// only combination TIM2+TIM5 implemented
#   ifdef CONFIG_STM32F7_HPTC_TICKTIMER_TIM2
#     define HPTC_AUXTIMER_ISSLAVE  1
#     define HPTC_AUXTIMER_TS GTIM_SMCR_ITR2
#   else
#	  define HPTC_AUXTIMER_ISSLAVE  0
#   endif

#   define HPTC_AUXTIMER_TYPE 2
#   define HPTC_AUXTIMER_OC_MASK 0x1E00
#   define HPTC_AUX_IRQ            STM32_IRQ_TIM5
#   define HPTC_AUXTIMER_BASE      STM32_TIM5_BASE
#   define HPTC_AUXTIMER_CNT       STM32_TIM5_CNT
#   define HPTC_AUXTIMER_SR        STM32_TIM5_SR
#   define HPTC_AUXTIMER_DIER      STM32_TIM5_DIER
#   define HPTC_AUXTIMER_CCMR1     STM32_TIM5_CCMR1
#   define HPTC_AUXTIMER_CCMR2     STM32_TIM5_CCMR2
#   define HPTC_AUXTIMER_RES       STM32F7_TIM5_HPTC_RES
#   define HPTC_AUXTIMER_CHANNELS  4
#endif


//TODO: stm32_tim_dev_channel_s.type use enum?
#define HPTC_TIMER_CHANNEL_TYPE_UNUSED    0
#define HPTC_TIMER_CHANNEL_TYPE_TICKTIMER 1
#define HPTC_TIMER_CHANNEL_TYPE_IC        2
#define HPTC_TIMER_CHANNEL_TYPE_OC        3


#ifndef NS_PER_SEC
#define NS_PER_SEC 1000000000
#endif


/****************************************************************************
 * Private Types
 ****************************************************************************/
/* This structure provides the private representation of the "lower-half"
 * driver state structure.  This structure must be cast-compatible with the
 * hptc_lowerhalf_s structure.
 */



struct stm32_tim_phys_dev_s
{
	FAR struct stm32_tim_phys_dev_s *next;
	const uint8_t                   resolution;
	bool                            isSlave; /*master or slave to sync multiple hptc timers*/
	uint16_t						ts;
	uint32_t						minPeriod; /*minimum Counter overflow period*/
	uint32_t                        base;
	uint8_t							type;
//	hptccb_t                          callback;   /* Current user interrupt callback */
};


struct stm32_lowerhalf_s
{
  FAR const struct hptc_ops_s *ops;        /* Lower half operations */
  FAR  struct stm32_tim_phys_dev_s *first;
//TODO: linked list for devices
//  FAR struct stm32_tim_dev_s   *tim;        /* stm32 timer driver */
  hptccb_t                        tick_cb;   /* Current user interrupt callback */
//  FAR void                     *arg;        /* Argument passed to upper half callback */
//  const xcpt_t                  timhandler; /* Current timer interrupt handler */
  bool							started;
  bool                          tick_started;    /* True: Timer has been started */
  bool							cnt_started; /* True: Counter has been started */
//  uint8_t                       resolution; /* Number of bits in the timer (16 or 32 bits) */
  uint32_t						frequency;
};



struct stm32_tim_dev_channel_s
{
	uint8_t type;  //TODO: use enum???
	uint32_t ccr;
	uint32_t sr_ccif;
	uint32_t sr_ccof;

};


struct stm32_timdev_s
{
    uint32_t tick_timer_ccmr1_shadow;
    uint32_t tick_timer_ccmr2_shadow;
    struct stm32_tim_dev_channel_s tick_timer_ch[HPTC_TICKTIMER_CHANNELS];
#ifdef HPTC_AUXTIMER
    uint32_t aux_timer_ccmr1_shadow;
    uint32_t aux_timer_ccmr2_shadow;
	struct stm32_tim_dev_channel_s aux_timer_ch[HPTC_AUXTIMER_CHANNELS];
#endif
};



#define TIMER_CCIF_FLAG 1
#define TIMER_CCOF_FLAG 2
#define TIMER_SET_FLAG 4
#define TIMER_SET_RISING_EDGE 8
#define TIMER_SET_FALLING_EDGE 16

struct stm32_hptc_channel_s{
	uint8_t mode;
	uint8_t flags;
	int32_t ticks;
};


struct stm32_hptc_tick_cmd_s
{
	uint32_t tick_cmd;
	uint32_t hptc_tim_ticks_per_hptc_tick_cmd;
	struct stm32_hptc_channel_s tick_timer_ch[HPTC_TICKTIMER_CHANNELS];
#ifdef HPTC_AUXTIMER
	struct stm32_hptc_channel_s aux_timer_ch[HPTC_AUXTIMER_CHANNELS];
#endif
};

struct stm32_hptc_tick_s
{
	uint32_t tick;
	int32_t  tick_s;
	int32_t  tick_ns;
	uint32_t hptc_tim_ticks;
	uint32_t hptc_irq_delay;
	uint32_t flags;
	uint32_t tick_timer_sr;
	uint32_t hptc_tim_ticks_per_hptc_tick;
	uint32_t hptc_tim_ticks_per_hptc_tick_prev;
	uint8_t  hptc_cmd_buffer_valid;
    int32_t tick_timer_capture[HPTC_TICKTIMER_CHANNELS];
#ifdef HPTC_AUXTIMER
	uint32_t aux_timer_sr;
	int32_t aux_timer_capture[HPTC_AUXTIMER_CHANNELS];
#endif
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Interrupt handling *******************************************************/

static int stm32_hptc_pendsv_handler(FAR struct stm32_lowerhalf_s *lower);

/* "Lower half" driver methods **********************************************/
static int stm32_cnt_start(FAR struct hptc_lowerhalf_s *lower);
static int stm32_cnt_stop(FAR struct hptc_lowerhalf_s *lower);
static int stm32_start(FAR struct hptc_lowerhalf_s *lower, sem_t* sem);
static int stm32_stop(FAR struct hptc_lowerhalf_s *lower);

static int stm32_getstatus(FAR struct hptc_lowerhalf_s *lower,
                        FAR struct hptc_status_s *status);
static int stm32_settimeout(FAR struct hptc_lowerhalf_s *lower,
                            uint32_t timeout);
static void stm32_setcallback(FAR struct hptc_lowerhalf_s *lower,
                              hptccb_t callback, FAR void *arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/
/* "Lower half" driver methods */


static const struct hptc_ops_s g_timer_ops =
{
  .start       = stm32_start,
  .stop        = stm32_stop,
  .getstatus   = stm32_getstatus,
  .settimeout  = stm32_settimeout,
  .setcallback = stm32_setcallback,
  .ioctl       = NULL,
};

static struct stm32_lowerhalf_s g_hptc_lowerhalf = {
		  .ops         = &g_timer_ops //,
//		  .timhandler  = stm32_tim1_interrupt,
//		  .resolution  = STM32F7_TIM1_HPTC_RES
		};

static uint32_t g_hptc_ns_per_hptc_tick=5000;//2kHz 250000; //FIXME: 4kHz hard coded
static uint32_t g_hptc_tim_ticks_per_hptc_tick=108000;//216000; //1kHz//42000; //??? FIXME:4kHz hard coded
static uint32_t g_hptc_tim_subticks_per_hptc_tick=0;  //???
static volatile uint32_t g_hptc_tim_ticks=0; //??
static volatile uint32_t g_hptc_tim_subticks=0; //???
static volatile uint32_t g_hptc_ticks=0;  //used
static volatile uint32_t g_debug_cnt2=2; //??


#define PEND_SV_STATE_IDLE 0
#define PEND_SV_STATE_ACTIVE 1
#define PEND_SV_STATE_TRIGGERED 2

static volatile uint32_t g_pend_sv_state=PEND_SV_STATE_IDLE;


static struct stm32_tim_phys_dev_s g_ticktimer_dev =
{
    .next = NULL,
	.resolution = HPTC_TICKTIMER_RES,
	.base = HPTC_TICKTIMER_BASE,
	.isSlave = HPTC_TICKTIMER_ISSLAVE,
#if HPTC_TICKTIMER_ISSLAVE
	.ts = HPTC_TICKTIMER_TS,
#endif
	.type = HPTC_TICKTIMER_TYPE


};


#ifdef HPTC_AUXTIMER
static struct stm32_tim_phys_dev_s g_auxtimer_dev =
{
    .next = NULL,
	.resolution = HPTC_AUXTIMER_RES,
	.base = HPTC_AUXTIMER_BASE,
	.isSlave = HPTC_AUXTIMER_ISSLAVE,
#if HPTC_AUXTIMER_ISSLAVE
	.ts =HPTC_AUXTIMER_TS,
#endif
	.type = HPTC_AUXTIMER_TYPE
};
#endif

#define HPTC_TICKBUFFER_SIZE (3*CONFIG_HPTC_HP_OVERSAMPLE)
#define HPTC_TICKBUFFER_CMD_SIZE (2*CONFIG_HPTC_HP_OVERSAMPLE)
static volatile struct stm32_hptc_tick_s g_hptc_tick_buffer[HPTC_TICKBUFFER_SIZE];

static volatile struct stm32_hptc_tick_cmd_s g_hptc_tick_buffer_cmd_A[CONFIG_HPTC_HP_OVERSAMPLE];
static volatile struct stm32_hptc_tick_cmd_s g_hptc_tick_buffer_cmd_B[CONFIG_HPTC_HP_OVERSAMPLE];

static volatile struct stm32_hptc_tick_cmd_s* volatile g_hptc_tick_buffer_cmd_valid=g_hptc_tick_buffer_cmd_B;



static volatile uint16_t g_hptc_tick_buffer_tick_handler_write_idx=0;
static volatile uint16_t g_hptc_tick_buffer_read_idx=0;//g_hptc_tick_buffer_pendsv_handler_idx;


static volatile uint32_t g_hptc_pend_sv_trigger_ticks;

static volatile struct stm32_timdev_s g_tim_dev;

static volatile sem_t* g_sem;


/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Name: stm32_hptc_tick_handler
 *
 * Description:
 *   hptc high priority interrupt handler
 *
 * Input Parameters:
 *
 * Returned Values:
 *
 ****************************************************************************/
void stm32_hptc_tick_handler(void)
{
	static uint32_t hptc_ticks=0; //upcounting ticks; was g_hptc_ticks
	static int32_t hptc_s; //uptime seconds
	static int32_t hptc_ns;//uptime nanoseconds

	static uint32_t missed_ticks_sum=0;
	static uint32_t hptc_oversample_slice=0;
#if HPTC_TICKTIMER_RES == 16
	static uint16_t hptc_tim_ticks_next;
	uint16_t hptc_tim_ticks;
	uint16_t hptc_tim_cnt;
#else
	static uint32_t hptc_tim_ticks_next;
	uint32_t hptc_tim_cnt;
	uint32_t hptc_tim_ticks;
#endif






	static uint32_t hptc_tim_ticks_per_hptc_tick=54000;//2000Hz XXXXXX 108000;//216000;//FIXME: hardcoded initial value


	static uint16_t hptc_tick_buffer_tick_handler_write_idx=0;
	static uint16_t hptc_tick_buffer_cmd_tick_handler_read_idx=0;
	static volatile struct stm32_hptc_tick_cmd_s* hptc_tick_buffer_cmd=g_hptc_tick_buffer_cmd_B; //non volatile pointer to volatile struct

	static uint8_t  buffer_available=0;
	uint8_t  cmd_buffer_valid=0;
	uint8_t  set_tick_timer_ccmr1=0;
	uint8_t  set_tick_timer_ccmr2=0;
#ifdef HPTC_AUXTIMER
	uint8_t  set_aux_timer_ccmr1=0;
	uint8_t  set_aux_timer_ccmr2=0;

#endif

	//add external update:
	//if(g_hptc_tick_handler_update_state == HPTC_TICK_HANDLER_UPDATE_PEND
	//g_hptc_ticks=
	//hptc_oversample_slice=X //or run external update at (hptc_oversample_slice==0)

	uint32_t missed_ticks=0;
	uint32_t hptc_tim_ticks_per_hptc_tick_prev=	hptc_tim_ticks_per_hptc_tick;

for(;;) {
	hptc_ticks++;
	hptc_ns+=g_hptc_ns_per_hptc_tick;
	if (hptc_ns>=NS_PER_SEC){
		hptc_ns-=NS_PER_SEC;
		hptc_s++;
	}

#if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
	hptc_oversample_slice++;
	if (hptc_oversample_slice == CONFIG_HPTC_HP_OVERSAMPLE) {
		hptc_oversample_slice = 0;
		//optional: run external update here
    }
#endif

	g_hptc_ticks=hptc_ticks; //copy to global variable





#if HPTC_TICKTIMER_RES == 16
    hptc_tim_ticks=getreg16(HPTC_TICKTIMER_CCR);   //ticks now
#else
    hptc_tim_ticks=getreg32(HPTC_TICKTIMER_CCR);   //ticks now
#endif

    //TODO: if(hptc_tim_ticks != hptc_tim_ticks_next) ==> ERROR, wrong triggered interrupt

#if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
	if (!hptc_oversample_slice){
#endif
		if(hptc_tick_buffer_cmd!=g_hptc_tick_buffer_cmd_valid){//new buffer available
			hptc_tick_buffer_cmd=g_hptc_tick_buffer_cmd_valid;
			buffer_available=1;
		} else {
			buffer_available=0;
		}
#if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
	}
#endif

	cmd_buffer_valid = (buffer_available && (g_hptc_tick_buffer_cmd_valid[hptc_oversample_slice].tick_cmd == hptc_ticks));


	if(cmd_buffer_valid){
        hptc_tim_ticks_per_hptc_tick=  hptc_tick_buffer_cmd[hptc_oversample_slice].hptc_tim_ticks_per_hptc_tick_cmd;
	}

    hptc_tim_ticks_next=hptc_tim_ticks+hptc_tim_ticks_per_hptc_tick;

#if HPTC_TICKTIMER_RES ==16
			putreg16(hptc_tim_ticks_next,HPTC_TICKTIMER_CCR); //set next irq trigger time
#else
			putreg32(hptc_tim_ticks_next,HPTC_TICKTIMER_CCR); //set next irq trigger time
#endif

   	// check for missed interrupt:   int32_t time_left = hptc_tim_ticks_next - hptc_tick_time getreg32(HPTC_TICKTIMER_CNT);

    putreg16(~(HPTC_TICKTIMER_SR_CCIF),HPTC_TICKTIMER_SR);  //clear pending CCIF bit to acknowledge interrupt


	#if HPTC_TICKTIMER_RES == 16
	    hptc_tim_cnt=getreg16(HPTC_TICKTIMER_CNT);
	    uint16_t delta_ticks;
	#else
	    hptc_tim_cnt=getreg32(HPTC_TICKTIMER_CNT);
	    uint32_t delta_ticks;
	#endif


	    delta_ticks=hptc_tim_ticks_next-hptc_tim_cnt;

	    if(delta_ticks<=hptc_tim_ticks_per_hptc_tick){  //timing okay, leave loop
	    	break;
	    }
	    missed_ticks++;
}

	if(missed_ticks){
		//TODO: set flags
		missed_ticks_sum+=missed_ticks;

	}



    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].hptc_irq_delay=hptc_tim_cnt-hptc_tim_ticks;



    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].hptc_tim_ticks_per_hptc_tick_prev=hptc_tim_ticks_per_hptc_tick_prev;
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].hptc_cmd_buffer_valid=cmd_buffer_valid;



    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].tick=hptc_ticks;
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].tick_s=hptc_s;
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].tick_ns=hptc_ns;
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].hptc_tim_ticks=hptc_tim_ticks;
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].hptc_tim_ticks_per_hptc_tick=hptc_tim_ticks_per_hptc_tick;
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].flags=cmd_buffer_valid;

    //int32_t hptc_tim_tick_delay=getreg32(HPTC_TICKTIMER_CNT)-g_hptc_tim_ticks;

    //get status register
     uint32_t ticktimer_sr=getreg16(HPTC_TICKTIMER_SR);
     putreg16(~(HPTC_TICKTIMER_OC_MASK & ticktimer_sr),HPTC_TICKTIMER_SR);  //clear overcapture flags
 #ifdef HPTC_AUXTIMER
     uint32_t auxtimer_sr=getreg16(HPTC_AUXTIMER_SR);
     uint32_t auxtimer_sr_write=(HPTC_AUXTIMER_OC_MASK & auxtimer_sr);
     if(auxtimer_sr_write) putreg16(~auxtimer_sr_write,HPTC_AUXTIMER_SR);   //clear overcapture flags
     auxtimer_sr_write=0;
 #endif




    for(int i=0;i<HPTC_TICKTIMER_CHANNELS;i++){
    	switch (g_tim_dev.tick_timer_ch[i].type){

    	case HPTC_TIMER_CHANNEL_TYPE_IC:
    		if (ticktimer_sr & g_tim_dev.tick_timer_ch[i].sr_ccif){
#if HPTC_TICKTIMER_RES ==16
    			g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].tick_timer_capture[i]=getreg16(g_tim_dev.tick_timer_ch[i].ccr)-hptc_tim_ticks;
#else
    			g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].tick_timer_capture[i]=getreg32(g_tim_dev.tick_timer_ch[i].ccr)-hptc_tim_ticks;
#endif

    		}
    		break;

     	case HPTC_TIMER_CHANNEL_TYPE_TICKTIMER:
     		//tick_timer_CCR already set
    		//already done *** ticktimer_sr_write |= ticktimer_sr & g_tim_dev.tick_timer_ch[i].sr_ccif; //prepare acknowledge irq
//#if HPTC_TICKTIMER_RES ==16
//			putreg16(hptc_tim_ticks_next,g_tim_dev.tick_timer_ch[i].ccr); //set next irq trigger time
//#else
//			putreg32(hptc_tim_ticks_next,g_tim_dev.tick_timer_ch[i].ccr); //set next irq trigger time
//#endif
    		if(cmd_buffer_valid){
    			if(hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].flags & TIMER_SET_RISING_EDGE){
    			if(i<=2){
    				g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<i);
    				g_tim_dev.tick_timer_ccmr1_shadow |=0x10<<i;
    				set_tick_timer_ccmr1=1;
    			} else {
    				g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<(i-2));
    				g_tim_dev.tick_timer_ccmr2_shadow |=0x10<<(i-2);
    				set_tick_timer_ccmr2=1;
    			}
    			} else if (hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].flags & TIMER_SET_FALLING_EDGE){
        			if(i<=2){
        				g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<i);
        				g_tim_dev.tick_timer_ccmr1_shadow |=0x20<<i;
        				set_tick_timer_ccmr1=1;
        			} else {
        				g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<(i-2));
        				g_tim_dev.tick_timer_ccmr2_shadow |=0x20<<(i-2);
        				set_tick_timer_ccmr2=1;
        			}
    			}
    		}


    		break;

    	case HPTC_TIMER_CHANNEL_TYPE_OC:
    		//*** NOT REQUIRED *** ticktimer_sr_write |= ticktimer_sr & g_tim_dev.tick_timer_ch[i].sr_ccif;  //prepare clear flag
    		if(cmd_buffer_valid && (hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].flags & TIMER_SET_FLAG)){
#if HPTC_TICKTIMER_RES ==16
    			putreg16(hptc_tim_ticks_next + hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].ticks ,g_tim_dev.tick_timer_ch[i].ccr);
#else
    			putreg32(hptc_tim_ticks_next + hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].ticks ,g_tim_dev.tick_timer_ch[i].ccr);
#endif
    		    if(hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].flags & TIMER_SET_RISING_EDGE){
    		    	if(i<=2){
    		    	    				g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<i);
    		    	    				g_tim_dev.tick_timer_ccmr1_shadow |=0x10<<i;
    		    	    				set_tick_timer_ccmr1=1;
    		    	    			} else {
    		    	    				g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<(i-2));
    		    	    				g_tim_dev.tick_timer_ccmr2_shadow |=0x10<<(i-2);
    		    	    				set_tick_timer_ccmr2=1;
    		    	    			}
    		    } else if (hptc_tick_buffer_cmd[hptc_oversample_slice].tick_timer_ch[i].flags & TIMER_SET_FALLING_EDGE){
        			if(i<=2){
        				g_tim_dev.tick_timer_ccmr1_shadow &=~(0x70<<i);
        				g_tim_dev.tick_timer_ccmr1_shadow |=0x20<<i;
        				set_tick_timer_ccmr1=1;
        			} else {
        				g_tim_dev.tick_timer_ccmr2_shadow &=~(0x70<<(i-2));
        				g_tim_dev.tick_timer_ccmr2_shadow |=0x20<<(i-2);
        				set_tick_timer_ccmr2=1;
        			}
    		    }
    		}
    		break;


    	}
    }

    if(set_tick_timer_ccmr1){
    	putreg32(HPTC_TICKTIMER_CCMR1,g_tim_dev.tick_timer_ccmr1_shadow);
    }
    if(set_tick_timer_ccmr2){
    	putreg32(HPTC_TICKTIMER_CCMR1,g_tim_dev.tick_timer_ccmr2_shadow);
    }

    ticktimer_sr |= getreg16(HPTC_TICKTIMER_SR) & HPTC_TICKTIMER_OC_MASK; //update overcapture
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].tick_timer_sr = ticktimer_sr;

#ifdef HPTC_AUXTIMER
    for(int i=0;i<HPTC_AUXTIMER_CHANNELS;i++){
    	switch (g_tim_dev.aux_timer_ch[i].type){
    	case HPTC_TIMER_CHANNEL_TYPE_IC:
    		if (auxtimer_sr & g_tim_dev.aux_timer_ch[i].sr_ccif){
#if HPTC_AUXTIMER_RES ==16
    			g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].aux_timer_capture[i]=getreg16(g_tim_dev.aux_timer_ch[i].ccr)-hptc_tim_ticks;
#else
    			g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].aux_timer_capture[i]=getreg32(g_tim_dev.aux_timer_ch[i].ccr)-hptc_tim_ticks;

#endif
    		}
    		break;
    	case HPTC_TIMER_CHANNEL_TYPE_OC:
    		//not required*** auxtimer_sr_write |= auxtimer_sr & g_tim_dev.aux_timer_ch[i].sr_ccif;
    		if(cmd_buffer_valid && (hptc_tick_buffer_cmd[hptc_oversample_slice].aux_timer_ch[i].flags & TIMER_SET_FLAG)){
#if HPTC_AUXTIMER_RES ==16
    			putreg16(hptc_tim_ticks_next + hptc_tick_buffer_cmd[hptc_oversample_slice].aux_timer_ch[i].ticks ,g_tim_dev.aux_timer_ch[i].ccr);
#else
    			putreg32(hptc_tim_ticks_next + hptc_tick_buffer_cmd[hptc_oversample_slice].aux_timer_ch[i].ticks ,g_tim_dev.aux_timer_ch[i].ccr);
#endif
    		    if(hptc_tick_buffer_cmd[hptc_oversample_slice].aux_timer_ch[i].flags & TIMER_SET_RISING_EDGE){
    		    	if(i<=2){
    		    	    				g_tim_dev.aux_timer_ccmr1_shadow &=~(0x70<<i);
    		    	    				g_tim_dev.aux_timer_ccmr1_shadow |=0x10<<i;
    		    	    				set_tick_timer_ccmr1=1;
    		    	    			} else {
    		    	    				g_tim_dev.aux_timer_ccmr2_shadow &=~(0x70<<(i-2));
    		    	    				g_tim_dev.aux_timer_ccmr2_shadow |=0x10<<(i-2);
    		    	    				set_tick_timer_ccmr2=1;
    		    	    			}
    		    } else if (hptc_tick_buffer_cmd[hptc_oversample_slice].aux_timer_ch[i].flags & TIMER_SET_FALLING_EDGE){
        			if(i<=2){
        				g_tim_dev.aux_timer_ccmr1_shadow &=~(0x70<<i);
        				g_tim_dev.aux_timer_ccmr1_shadow |=0x20<<i;
        				set_aux_timer_ccmr1=1;
        			} else {
        				g_tim_dev.aux_timer_ccmr2_shadow &=~(0x70<<(i-2));
        				g_tim_dev.aux_timer_ccmr2_shadow |=0x20<<(i-2);
        				set_aux_timer_ccmr2=1;
        			}
    		    }
    		}



    		break;


    	}
    }

    if(set_aux_timer_ccmr1){
    	putreg32(HPTC_AUXTIMER_CCMR1,g_tim_dev.aux_timer_ccmr1_shadow);
    }
    if(set_aux_timer_ccmr2){
    	putreg32(HPTC_AUXTIMER_CCMR1,g_tim_dev.aux_timer_ccmr2_shadow);
    }

    auxtimer_sr |= getreg16(HPTC_AUXTIMER_SR) & HPTC_AUXTIMER_OC_MASK; //update overcapture
    g_hptc_tick_buffer[hptc_tick_buffer_tick_handler_write_idx].aux_timer_sr = auxtimer_sr;

#endif
    //process slice




    //setup next slice
//	g_hptc_tim_subticks+=g_hptc_tim_subticks_per_hptc_tick;
//	g_hptc_tim_ticks += (g_hptc_tim_subticks<g_hptc_tim_subticks_per_hptc_tick); //carry bit from subticks
//	g_hptc_tim_ticks += g_hptc_tim_ticks_per_hptc_tick;


	  // acknowledge interrupt
//	  putreg16(~HPTC_TICKTIMER_SR_CCIF, HPTC_TICKTIMER_SR);
	  //set new tick_cnt
//	  putreg32(g_hptc_tim_ticks,HPTC_TICKTIMER_CCR);

/*
      g_ic0=getreg32(HPTC_IC0_CCR);
      g_ic1=getreg32(HPTC_IC0_CCR);
      g_ic2=getreg32(HPTC_IC0_CCR);
      ticktimer_sr2=getreg16(HPTC_TICKTIMER_SR);
      putreg16(~ticktimer_sr2 | ~(HPTC_IC0_SR_CCOF | HPTC_IC1_SR_CCOF | HPTC_IC2_SR_CCOF), HPTC_IC012_SR);//clear overcapture flags in ticktimer_sr2

*/




    uint16_t temp_write_idx;
    temp_write_idx=hptc_tick_buffer_tick_handler_write_idx+1;
	if (temp_write_idx>=HPTC_TICKBUFFER_SIZE){
		temp_write_idx=0;
	}

	if(temp_write_idx != g_hptc_tick_buffer_read_idx){        //use next index if space left in buffer
		hptc_tick_buffer_tick_handler_write_idx=temp_write_idx;
	}

	  //trigger PendSV
#if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
	if (!hptc_oversample_slice){
#endif

		//if(g_pend_sv_state != PEND_SV_STATE_IDLE) {} //ERROR: missed pend_sv

    	  if(g_pend_sv_state != PEND_SV_STATE_ACTIVE){
    	  //if g_hptc_pend_idle ... else error++
    	  g_hptc_tick_buffer_tick_handler_write_idx=hptc_tick_buffer_tick_handler_write_idx;
    	  g_hptc_pend_sv_trigger_ticks=hptc_ticks;
    	  g_pend_sv_state = PEND_SV_STATE_TRIGGERED;
 	      putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL); /*ICSR!!!*/
    	  } else {
    		  //ERROR missed pendsv
    	  }
#if (CONFIG_HPTC_HP_OVERSAMPLE > 1)
      }
#endif




}





/****************************************************************************
 * Name: stm32_hptc_pendsv_handler
 *
 * Description:
 *   hptc pendsv interrupt handler
 *
 * Input Parameters:
 *
 * Returned Values:
 *
 ****************************************************************************/

static int stm32_hptc_pendsv_handler(FAR struct stm32_lowerhalf_s *lower)
{
	static uint16_t hptc_tick_buffer_read_idx=0;
	static uint32_t last_ticks=0;
	static uint32_t last_read_tick=0;
    static volatile struct stm32_hptc_tick_cmd_s* hptc_tick_buffer_cmd_write=g_hptc_tick_buffer_cmd_A;
    uint32_t ticks;
    static int32_t time_s=0;
    static int32_t time_ns=0;


    g_pend_sv_state=PEND_SV_STATE_ACTIVE;
    ticks=g_hptc_pend_sv_trigger_ticks;
    //check for missed calls, count error

	if(last_ticks!=ticks){
		//ERROR MISSED TICKS for input capture
		//set flag, count errors, ...
	}


    //get data([1,...Oversample-1,0]


	while(hptc_tick_buffer_read_idx != g_hptc_tick_buffer_tick_handler_write_idx){

		uint32_t tick=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].tick;
		time_s=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].tick_s;
		time_ns=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].tick_ns;
		uint32_t tick_timer_sr=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].tick_timer_sr;
		uint32_t tick_flags=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].flags;//ERROR  FLAGS
		uint32_t hptc_tim_ticks=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].hptc_tim_ticks; //debug only???

		if((tick-last_read_tick) > 1){
			//ERROR MISSED TICKS for input capture
			//set flag, count errors, ...
		}

	    for(int i=0;i<HPTC_TICKTIMER_CHANNELS;i++){
	    	if(g_tim_dev.tick_timer_ch[i].type == HPTC_TIMER_CHANNEL_TYPE_IC){
	    		if (tick_timer_sr & g_tim_dev.tick_timer_ch[i].sr_ccof){
	    			//overcapture
	    			//...add -1 for error to read buffer
	    		}
	    		else if (tick_timer_sr & g_tim_dev.tick_timer_ch[i].sr_ccif){
	    			int32_t capture=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].tick_timer_capture[i];
	    			float capture_subtick;
		    		if(capture<0){
		    			capture_subtick= (float)capture / (float)g_hptc_tick_buffer[hptc_tick_buffer_read_idx].hptc_tim_ticks_per_hptc_tick_prev;
		    		} else if (capture>0) {
		    			capture_subtick= (float)capture /  (float)g_hptc_tick_buffer[hptc_tick_buffer_read_idx].hptc_tim_ticks_per_hptc_tick;
		    		} else {
		    		    capture_subtick= 0.0;
		    		}
		    		int capture_ns=capture_subtick * g_hptc_ns_per_hptc_tick+ time_ns;
		    		int capture_s= time_s;
		    		if (capture_ns<0){
		    			capture_ns+=NS_PER_SEC;
		    			capture_s--;
		    		} else if (capture_ns>NS_PER_SEC){
		    			capture_ns-=NS_PER_SEC;
		    			capture_s++;

		    		}
		    		//add capture_s and captuer_ns to read buffer
	    		}

	    	}
	    }


#ifdef HPTC_AUXTIMER
	uint32_t aux_timer_sr=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].aux_timer_sr;

    for(int i=0;i<HPTC_AUXTIMER_CHANNELS;i++){
    	if(g_tim_dev.aux_timer_ch[i].type == HPTC_TIMER_CHANNEL_TYPE_IC){
    		if (aux_timer_sr & g_tim_dev.aux_timer_ch[i].sr_ccof){
    			//overcapture
    			//...add -1 for error to read buffer
    		}
    		else if (aux_timer_sr & g_tim_dev.aux_timer_ch[i].sr_ccif){
    			int32_t capture=g_hptc_tick_buffer[hptc_tick_buffer_read_idx].aux_timer_capture[i];
    			float capture_subtick;
	    		if(capture<0){
	    			capture_subtick= (float)capture / (float)g_hptc_tick_buffer[hptc_tick_buffer_read_idx].hptc_tim_ticks_per_hptc_tick_prev;
	    		} else if (capture>0) {
	    			capture_subtick= (float)capture /  (float)g_hptc_tick_buffer[hptc_tick_buffer_read_idx].hptc_tim_ticks_per_hptc_tick;
	    		} else {
	    		    capture_subtick= 0.0;
	    		}
	    		int capture_ns=capture_subtick * g_hptc_ns_per_hptc_tick+ time_ns;
	    		int capture_s=time_s;
	    		if (capture_ns<0){
	    			capture_ns+=NS_PER_SEC;
	    			capture_s--;
	    		} else if (capture_ns>NS_PER_SEC){
	    			capture_ns-=NS_PER_SEC;
	    			capture_s++;
	    		}
	    		//add capture_s and captuer_ns to read buffer
    		}

    	}
    }



#endif



        last_read_tick=tick;

		//set next read index
		if(hptc_tick_buffer_read_idx<(HPTC_TICKBUFFER_SIZE-1)){
			hptc_tick_buffer_read_idx++;
		} else {
			hptc_tick_buffer_read_idx=0;
		}


	}
	g_hptc_tick_buffer_read_idx=hptc_tick_buffer_read_idx; //update global read_idx



	//setup hptc_tick_buffer


	uint32_t tick_cmd=ticks+CONFIG_HPTC_HP_OVERSAMPLE+1;

    for (int i=0; i<(CONFIG_HPTC_HP_OVERSAMPLE); i++){
       	hptc_tick_buffer_cmd_write[i].tick_cmd=tick_cmd+i;
       	hptc_tick_buffer_cmd_write[i].hptc_tim_ticks_per_hptc_tick_cmd=g_hptc_tim_ticks_per_hptc_tick;

       	for (int j=0; j<HPTC_TICKTIMER_CHANNELS; j++){
       		hptc_tick_buffer_cmd_write[i].tick_timer_ch[j].mode=0;
       		hptc_tick_buffer_cmd_write[i].tick_timer_ch[j].flags=0;
       		hptc_tick_buffer_cmd_write[i].tick_timer_ch[j].ticks=0;

       	}
#ifdef HPTC_AUXTIMER
       	for (int j=0; j<HPTC_AUXTIMER_CHANNELS; j++){
       		hptc_tick_buffer_cmd_write[i].tick_timer_ch[j].mode=0;
       		hptc_tick_buffer_cmd_write[i].tick_timer_ch[j].flags=0;
       		hptc_tick_buffer_cmd_write[i].aux_timer_ch[j].ticks=0;
       	}

#endif //HPTC_AUXTIMER

    }

    volatile struct stm32_hptc_tick_cmd_s* g_hptc_tick_buffer_cmd_tmp =g_hptc_tick_buffer_cmd_valid;
    g_hptc_tick_buffer_cmd_valid = hptc_tick_buffer_cmd_write;
    hptc_tick_buffer_cmd_write = g_hptc_tick_buffer_cmd_tmp;



	//TODO: set semaphore to wake up related tasks.
    int sem_val=0;
    sem_getvalue(g_sem,&sem_val);
    if(sem_val<1)sem_post(g_sem);


g_debug_cnt2++;


#if 0
	uint32_t next_interval_us = 0;

  STM32F7_TIM_ACKINT(lower->tim, 0);

  if (lower->callback(&next_interval_us, lower->arg))
    {
      if (next_interval_us > 0)
        {
          STM32F7_TIM_SETPERIOD(lower->tim, next_interval_us);
        }
    }
  else
    {
      stm32_stop((struct hptc_lowerhalf_s *)lower);
    }
#endif


#ifdef HPTC_TRIGGER_SYSTICK
// trigger systick or run sched_process_timer()
//
#if 1
  //trigger systick
  //FIXME: 2000Hz hptc, 1000Hz systick hardcoded
//XXX  {
//XXX	  static int prescaler=1;
//XXX	  if(prescaler){
		  putreg32( NVIC_INTCTRL_PENDSTSET,NVIC_INTCTRL); /*ICSR!!!*/
//XXX		  prescaler=0;
//XXX	  } else prescaler=1;
//XXX  }

#else
  /* Process timer interrupt */

  sched_process_timer();

#endif
#endif

  last_ticks=ticks;
  g_pend_sv_state=PEND_SV_STATE_IDLE;
  return OK;
}




/************************************************************************************
 * Name: stm32_apb_tim_enable
 ************************************************************************************/

void stm32_apb_tim_enable()
{
  /* enable power */

#ifdef CONFIG_STM32F7_TIM1_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM1EN);
#endif
#ifdef CONFIG_STM32F7_TIM2_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM2EN);
#endif
#ifdef CONFIG_STM32F7_TIM3_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM3EN);
#endif
#ifdef CONFIG_STM32F7_TIM4_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM4EN);
#endif
#ifdef CONFIG_STM32F7_TIM5_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM5EN);
#endif

#ifdef CONFIG_STM32F7_TIM8_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM8EN);
#endif
#ifdef CONFIG_STM32F7_TIM9_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM9EN);
#endif
#ifdef CONFIG_STM32F7_TIM10_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM10EN);
#endif
#ifdef CONFIG_STM32F7_TIM11_HPTC
        modifyreg32(STM32F7_RCC_APB2ENR, 0, RCC_APB2ENR_TIM11EN);
#endif
#ifdef CONFIG_STM32F7_TIM12_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM12EN);
#endif
#ifdef CONFIG_STM32F7_TIM13_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM13EN);
#endif
#ifdef CONFIG_STM32F7_TIM14_HPTC
        modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM14EN);
#endif
#ifdef CONFIG_STM32F7_TIM15_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM15EN);
#endif
#ifdef CONFIG_STM32F7_TIM16_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM16EN);
#endif
#ifdef CONFIG_STM32F7_TIM17_HPTC
        modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM17EN);
#endif

}

  /************************************************************************************
   * Name: stm32_apb_tim_disable
   ************************************************************************************/

  void stm32_apb_tim_disable()
  {

    /* Disable power */

  #ifdef CONFIG_STM32F7_TIM1_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM1EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM2_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM2EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM3_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM3EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM4_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM4EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM5_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM5EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM8_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM8EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM9_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM9EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM10_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM10EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM11_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM11EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM12_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM12EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM13_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM13EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM14_HPTC
          modifyreg32(STM32_RCC_APB1ENR, RCC_APB1ENR_TIM14EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM15_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM15EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM16_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM16EN, 0);
  #endif
  #ifdef CONFIG_STM32F7_TIM17_HPTC
          modifyreg32(STM32_RCC_APB2ENR, RCC_APB2ENR_TIM17EN, 0);
  #endif

}

  /************************************************************************************
   * Name: stm32_init_timer
   ************************************************************************************/

void stm32_init_timer(FAR struct stm32_tim_phys_dev_s *first)
{
	struct stm32_tim_phys_dev_s *dev = first;
	while (dev!=NULL){
		uint16_t reg16;
		uint32_t reg32;

           /* Set up the timer CR1 register:
		   *
		   * 1,8   CKD[1:0] ARPE CMS[1:0] DIR OPM URS UDIS CEN
		   * 2-5   CKD[1:0] ARPE CMS      DIR OPM URS UDIS CEN
		   * 9-14  CKD[1:0] ARPE                  URS UDIS CEN
		   * 15-17 CKD[1:0] ARPE              OPM URS UDIS CEN
		   */
		reg16 = ATIM_CR1_UDIS;

		putreg16 (reg16, dev->base + STM32_ATIM_CR1_OFFSET);

        /* Set up the timer CR2 register:
		   *
		   * 1,8   OIS4 OIS3N OIS3 OIS2N OIS2 OIS1N OIS1 TI1S MMS[2:0] CCDS CCUS CCPC
		   * 2-5                                         TI1S MMS[2:0] CCDS
		   */
		if(dev->type == 1 || dev->type == 2 ){
		  reg16 = ATIM_CR2_MMS_ENABLE;
		  putreg16 (reg16, dev->base + STM32_ATIM_CR2_OFFSET);
		}

		//slave: set trigger mode; only start is controlled
		reg16= dev->ts | (dev->isSlave)? ATIM_SMCR_TRIGGER : 0x00;
		putreg16 (reg16, dev->base + STM32_ATIM_SMCR_OFFSET);

		if(dev->resolution == 16){
			putreg16(0xFFFF,dev->base + STM32_ATIM_ARR_OFFSET);
		}

		if(dev->resolution == 32){
			putreg32(0xFFFFFFFF,dev->base + STM32_ATIM_ARR_OFFSET);
		}

		//set prescaler
		putreg16(0x0, dev->base+STM32_ATIM_PSC_OFFSET); //TODO: HPTC: setup prescaler



		dev=dev->next;
	}

	//enable slave
	dev=first;
	while (dev!=NULL){
		if(dev->isSlave){
			modifyreg16(dev->base + STM32_ATIM_CR1_OFFSET, 0, ATIM_CR1_CEN);
		}

		dev=dev->next;
	}

	//enable master
	dev=first;
	while (dev!=NULL){
		if(!dev->isSlave){
			modifyreg16(dev->base + STM32_ATIM_CR1_OFFSET, 0, ATIM_CR1_CEN);
		}

		dev=dev->next;
	}



}





/****************************************************************************
 * Name: stm32_cnt_start
 *
 * Description:
 *   Start synchronized counters
 *
 * Input Parameters:
 *   lower - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_cnt_start(FAR struct hptc_lowerhalf_s *lower)
{
  FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

  if (!priv->cnt_started)
    {
	  stm32_apb_tim_enable();

	  //configure timer
      stm32_init_timer(priv->first);

      priv->cnt_started = true;
      return OK;
    }

  /* Return EBUSY to indicate that the timer was already running */

  return -EBUSY;
}



void stm32_tim_reset(){
	//TODO

#if 0






	  pwminfo("TIM%u\n", priv->timid);

	  /* Determine which timer to reset */

	  switch (priv->timid)
	    {
	#ifdef CONFIG_STM32F7_TIM1_PWM
	      case 1:
	        regaddr  = TIMRCCRST_TIM1;
	        resetbit = TIMRST_TIM1;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM2_PWM
	      case 2:
	        regaddr  = TIMRCCRST_TIM2;
	        resetbit = TIMRST_TIM2;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM3_PWM
	      case 3:
	        regaddr  = TIMRCCRST_TIM3;
	        resetbit = TIMRST_TIM3;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM4_PWM
	      case 4:
	        regaddr  = TIMRCCRST_TIM4;
	        resetbit = TIMRST_TIM4;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM5_PWM
	      case 5:
	        regaddr  = TIMRCCRST_TIM5;
	        resetbit = TIMRST_TIM5;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM8_PWM
	      case 8:
	        regaddr  = TIMRCCRST_TIM8;
	        resetbit = TIMRST_TIM8;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM9_PWM
	      case 9:
	        regaddr  = TIMRCCRST_TIM9;
	        resetbit = TIMRST_TIM9;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM10_PWM
	      case 10:
	        regaddr  = TIMRCCRST_TIM10;
	        resetbit = TIMRST_TIM10;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM11_PWM
	      case 11:
	        regaddr  = TIMRCCRST_TIM11;
	        resetbit = TIMRST_TIM11;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM12_PWM
	      case 12:
	        regaddr  = TIMRCCRST_TIM12;
	        resetbit = TIMRST_TIM12;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM13_PWM
	      case 13:
	        regaddr  = TIMRCCRST_TIM13;
	        resetbit = TIMRST_TIM13;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM14_PWM
	      case 14:
	        regaddr  = TIMRCCRST_TIM14;
	        resetbit = TIMRST_TIM14;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM15_PWM
	      case 15:
	        regaddr  = TIMRCCRST_TIM15;
	        resetbit = TIMRST_TIM15;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM16_PWM
	      case 16:
	        regaddr  = TIMRCCRST_TIM16;
	        resetbit = TIMRST_TIM16;
	        break;
	#endif
	#ifdef CONFIG_STM32F7_TIM17_PWM
	      case 17:
	        regaddr  = TIMRCCRST_TIM17;
	        resetbit = TIMRST_TIM17;
	        break;
	#endif
	      default:
	        return -EINVAL;
	    }

	  /* Disable interrupts momentary to stop any ongoing timer processing and
	   * to prevent any concurrent access to the reset register.
	   */

	  flags = enter_critical_section();

	  /* Stopped so frequency is zero */

	  priv->frequency = 0;

	  /* Disable further interrupts and stop the timer */

	  pwm_putreg(priv, STM32F7_GTIM_DIER_OFFSET, 0);
	  pwm_putreg(priv, STM32F7_GTIM_SR_OFFSET, 0);

	  /* Reset the timer - stopping the output and putting the timer back
	   * into a state where pwm_start() can be called.
	   */

	  regval  = getreg32(regaddr);
	  regval |= resetbit;
	  putreg32(regval, regaddr);

	  regval &= ~resetbit;
	  putreg32(regval, regaddr);
	  leave_critical_section(flags);

	  pwminfo("regaddr: %08x resetbit: %08x\n", regaddr, resetbit);
	  pwm_dumpregs(priv, "After stop");


#endif




}


/****************************************************************************
 * Name: stm32_cnt_stop
 *
 * Description:
 *   Stop and reset the timer resources
 *
 * Input parameters:
 *   dev - A reference to the lower half driver state structure
 *
 * Returned Value:
 *   Zero on success; a negated errno value on failure
 *
 * Assumptions:
 *   This function is called to stop the pulsed output at anytime.  This
 *   method is also called from the timer interrupt handler when a repetition
 *   count expires... automatically stopping the timer.
 *
 ****************************************************************************/

static int stm32_cnt_stop(FAR struct hptc_lowerhalf_s *lower)
{

//  FAR struct stm32_pwmtimer_s *priv = (FAR struct stm32_pwmtimer_s *)dev;
//  uint32_t resetbit;
//  uint32_t regaddr;
//  uint32_t regval;
//  irqstate_t flags;

    FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;


	if(priv->cnt_started){

      if(priv->tick_started){
		//TODO: stm32_tick_stop
	  }

      priv->cnt_started=false;

      stm32_tim_reset();

      stm32_apb_tim_disable();

	}



  return OK;
}


/****************************************************************************
 * Name: stm32_tick_start
 *
 * Description:
 *   Start HPTC tick and interrupt
 *
 * Input Parameters:
 *   lower - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_tick_start(FAR struct hptc_lowerhalf_s *lower)
{
  FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

  if (!priv->tick_started)
    {


    	   //set max timeout to avoid match during setup
    	   putreg32(getreg32(HPTC_TICKTIMER_CNT),HPTC_TICKTIMER_CCR);
    	   // clear pending interrupt
    	   putreg16(~HPTC_TICKTIMER_SR_CCIF, HPTC_TICKTIMER_SR);
    	   // enable capture compare interrupt
           putreg16(HPTC_TICKTIMER_DIER_CCIE, HPTC_TICKTIMER_DIER);

     	  //set first tick_cnt
          g_hptc_tim_ticks=getreg32(HPTC_TICKTIMER_CNT)+g_hptc_tim_ticks_per_hptc_tick;
      	  putreg32(g_hptc_tim_ticks,HPTC_TICKTIMER_CCR);
      	  g_hptc_tim_subticks=g_hptc_tim_subticks_per_hptc_tick;

#ifdef HPTC_TRIGGER_SYSTICK
      //workaround: disable arm counter
			//disable systick, triggered by HPTC from now on

      	  /* Enable SysTick interrupts:
      	   *
      	   *   NVIC_SYSTICK_CTRL_CLKSOURCE=0 : Use the implementation defined clock
      	   *                                   source which, for the STM32F7, will be
      	   *                                   HCLK/8
      	   *   NVIC_SYSTICK_CTRL_TICKINT=1   : Generate interrupts
      	   *   NVIC_SYSTICK_CTRL_ENABLE      : Enable the counter
      	   */

			putreg32(0,NVIC_SYSTICK_CTRL);

#endif

    	  up_enable_irq(HPTC_TICK_IRQ);



      priv->tick_started = true;
      return OK;
    }

  /* Return EBUSY to indicate that the timer was already running */

  return -EBUSY;
}





/****************************************************************************
 * Name: stm32_start
 *
 * Description:
 *   Start the timer, resetting the time to the current timeout,
 *
 * Input Parameters:
 *   lower - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_start(FAR struct hptc_lowerhalf_s *lower, sem_t *sem)
{


  FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

  if (!priv->started)
    {
	  g_sem = sem;
#if 0
      STM32F7_TIM_SETMODE(priv->tim, STM32F7_TIM_MODE_UP);

      if (priv->tick_cb != NULL)
        {
          STM32F7_TIM_SETISR(priv->tim, priv->timhandler, 0);
          STM32F7_TIM_ENABLEINT(priv->tim, 0);
        }
#endif
  	  stm32_cnt_start(lower);
  	  stm32_tick_start(lower);


      priv->started = true;
      return OK;
    }

  /* Return EBUSY to indicate that the timer was already running */

  return -EBUSY;
}

/****************************************************************************
 * Name: stm32_stop
 *
 * Description:
 *   Stop the timer
 *
 * Input Parameters:
 *   lower - A pointer the publicly visible representation of the "lower-half"
 *           driver state structure.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_stop(struct hptc_lowerhalf_s *lower)
{
#if 0
  struct stm32_lowerhalf_s *priv = (struct stm32_lowerhalf_s *)lower;

  if (priv->started)
    {
      STM32F7_TIM_SETMODE(priv->tim, STM32F7_TIM_MODE_DISABLED);
      STM32F7_TIM_DISABLEINT(priv->tim, 0);
      STM32F7_TIM_SETISR(priv->tim, 0, 0);
      priv->started = false;
      return OK;
    }

  /* Return ENODEV to indicate that the timer was not running */
#endif
  return -ENODEV;
}

/****************************************************************************
 * Name: stm32_settimeout
 *
 * Description:
 *   Set a new timeout value (and reset the timer)
 *
 * Input Parameters:
 *   lower   - A pointer the publicly visible representation of the "lower-half"
 *             driver state structure.
 *   timeout - The new timeout value in microseconds.
 *
 * Returned Values:
 *   Zero on success; a negated errno value on failure.
 *
 ****************************************************************************/

static int stm32_settimeout(FAR struct hptc_lowerhalf_s *lower, uint32_t timeout)
{
#if 0
  FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;
  uint64_t maxtimeout;

  if (priv->started)
    {
      return -EPERM;
    }

  maxtimeout = (1 << priv->resolution) - 1;
  if (timeout > maxtimeout)
    {
      uint64_t freq = (maxtimeout * 1000000) / timeout;
      STM32F7_TIM_SETCLOCK(priv->tim, freq);
      STM32F7_TIM_SETPERIOD(priv->tim, maxtimeout);
    }
  else
    {
      STM32F7_TIM_SETCLOCK(priv->tim, 1000000);
      STM32F7_TIM_SETPERIOD(priv->tim, timeout);
    }
#endif
  return OK;
}

static int stm32_getstatus(FAR struct hptc_lowerhalf_s *lower, FAR struct hptc_status_s *status){

	status->flags=getreg32(HPTC_TICKTIMER_CNT);
	status->cnt1=g_hptc_ticks;
	status->cnt2=g_debug_cnt2; //getreg32(HPTC_TICKTIMER_CCR); //

	return OK;
}


/****************************************************************************
 * Name: stm32_setcallback
 *
 * Description:
 *   Call this user provided timeout callback.
 *
 * Input Parameters:
 *   lower      - A pointer the publicly visible representation of the "lower-half"
 *                driver state structure.
 *   callback - The new timer expiration function pointer.  If this
 *                function pointer is NULL, then the reset-on-expiration
 *                behavior is restored,
 *  arg          - Argument that will be provided in the callback
 *
 * Returned Values:
 *   The previous timer expiration function pointer or NULL is there was
 *   no previous function pointer.
 *
 ****************************************************************************/

static void stm32_setcallback(FAR struct hptc_lowerhalf_s *lower,
                              hptccb_t callback, FAR void *arg)
{
#if 0
  FAR struct stm32_lowerhalf_s *priv = (FAR struct stm32_lowerhalf_s *)lower;

  irqstate_t flags = enter_critical_section();

  /* Save the new callback */

  priv->callback = callback;
  priv->arg      = arg;

  if (callback != NULL && priv->started)
    {
      STM32F7_TIM_SETISR(priv->tim, priv->timhandler, 0);
      STM32F7_TIM_ENABLEINT(priv->tim, 0);
    }
  else
    {
      STM32F7_TIM_DISABLEINT(priv->tim, 0);
      STM32F7_TIM_SETISR(priv->tim, 0, 0);
    }

  leave_critical_section(flags);
#endif
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_htpc_initialize
 *
 * Description:
 *   Bind the configuration hptc to a hptc lower half instance and
 *   register the device
 *
 * Returned Values:
 *   Zero (OK) is returned on success; A negated errno value is returned
 *   to indicate the nature of any failure.
 *
 ****************************************************************************/

int stm32_hptc_initialize()
{
  FAR struct stm32_lowerhalf_s *lower;

  lower = &g_hptc_lowerhalf;

//  lower->first = NULL;
//  FAR  struct stm32_tim_phys_dev_s *dev=NULL;

  lower->first = &g_ticktimer_dev;

#ifdef CONFIG_STM32F7_HPTC_AUXTIMER_NONE
  g_ticktimer_dev.next=NULL;
#else
  g_auxtimer_dev.next=NULL;
  g_ticktimer_dev.next=&g_auxtimer_dev;
#endif


//TODO ?!
//  g_tim_dev.tick_timer_ccmr1_shadow=
//  g_tim_dev.tick_timer_ccmr2_shadow=
//  g_tim_dev.aux_timer_ccmr1_shadow=
//  g_tim_dev.aux_timer_ccmr2_shadow=






  //attach and prioritize high priority hptc tick handler

  int ret;


  ret = up_ramvec_attach(HPTC_TICK_IRQ, stm32_hptc_tick_handler);
  if (ret < 0)
  {
    fprintf(stderr, "highpri_main: ERROR: up_ramvec_attach failed: %d\n", ret);
 	return -EPERM;
  }

  /* Set the priority of the HPTC interrupt vector */

  ret = up_prioritize_irq(HPTC_TICK_IRQ, NVIC_SYSH_HIGH_PRIORITY);
  if (ret < 0)
  {
  //todo: detach vector
    fprintf(stderr, "highpri_main: ERROR: up_prioritize_irq failed: %d\n", ret);
    return -EPERM;
  }

  irq_attach(STM32_IRQ_PENDSV,(xcpt_t )stm32_hptc_pendsv_handler, NULL);
  up_enable_irq(STM32_IRQ_PENDSV);



  /* Initialize the elements of lower half state structure */

//  lower->started  = false;
//  lower->callback = NULL;
//  lower->tim      = stm32_tim_init(timer);

  if (lower->first == NULL)
    {
      return -EINVAL;
    }




  /* Register the timer driver as /dev/timerX.  The returned value from
   * timer_register is a handle that could be used with timer_unregister().
   * REVISIT: The returned handle is discard here.
   */

  FAR void *drvr = hptc_register(CONFIG_HPTC_DEV_PATH,
                                  (FAR struct hptc_lowerhalf_s *)lower);
  if (drvr == NULL)
    {
      /* The actual cause of the failure may have been a failure to allocate
       * perhaps a failure to register the timer driver (such as if the
       * 'depath' were not unique).  We know here but we return EEXIST to
       * indicate the failure (implying the non-unique devpath).
       */

      return -EEXIST;
    }

  return OK;
}

#endif /* CONFIG_HPTC */
