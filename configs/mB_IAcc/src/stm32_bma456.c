/************************************************************************************
 * configs/mB_IAcc/src/stm32_bma456190.c
 *
 
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/


#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/sensors/bma456.h>

#include <stdio.h>
#include "stm32_spi.h"
#include "mB_IAcc.h"


#ifdef CONFIG_SENSORS_BMA456

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_bma456_initialize
 *
 * Description:
 *   Initialize SPI and start the register function from the BMA456 driver.
 *   register path /dev/BMA456_"num"
 *
 * Input parameters:
 *   num - the number of the driver
 *
 * Returned Value:
 *   Zero (OK) on success; a -1 value on failure.
 *
 ************************************************************************************/

int stm32_bma456_initialize(int num)
{
  FAR struct spi_dev_s *spi;
  int ret = 0;
  FAR const char *devpath;
  char devname[14];

  /*Initialize the SPI driver*/
  spi = stm32_spibus_initialize(1);

  if (!spi)
  {
	  return -1;
  }
  /*register the ad5662 character driver */
  sprintf(devname,"/dev/BMA456_%x",num);

  devpath = devname;

  bma456_register(devpath,spi,num);
  return ret;
}

#endif /* CONFIG_SPI && CONFIG_BMA456 */

