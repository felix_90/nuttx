/************************************************************************************
 * configs/mb_imu/src/stm32_boot.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            David Sidrane <david_s5@nscdg.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>

#include <nuttx/board.h>
#include <arch/board/board.h>

#include "up_arch.h"
#include "mb_imu.h"

#include "stm32_gpio.h" /* gpio initialization */

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{
	/* initialize some GPIOS */
	stm32_configgpio(GPIO_CAN_RS); //init GPIO to disable CAN transceiver
	stm32_configgpio(GPIO_MB_IMU_ADIS16XXX_POWER); //init GPIO to enable IMU powerswitch


#ifdef CONFIG_ARCH_LEDS
  /* Configure on-board LEDs if LED support has been selected. */

  board_autoled_initialize();
#endif

#if defined(CONFIG_STM32F7_OTGHS) || defined(CONFIG_STM32F7_HOST)
  stm32_usbinitialize();
#endif

#if defined(CONFIG_SPI)
  /* Configure SPI chip selects */

  stm32_spidev_initialize();
#endif





}

/************************************************************************************
 * Name: board_initialize
 *
 * Description:
 *   If CONFIG_BOARD_INITIALIZE is selected, then an additional initialization call
 *   will be performed in the boot-up sequence to a function called
 *   board_initialize().  board_initialize() will be called immediately after
 *   up_initialize() is called and just before the initial application is started.
 *   This additional initialization phase may be used, for example, to initialize
 *   board-specific device drivers.
 *
 ************************************************************************************/

#ifdef CONFIG_BOARD_INITIALIZE
void board_initialize(void)
{

	  int ret;

	#if 1
	#ifdef CONFIG_HPTC
	  /* Initialize and register the HPTC device. */
	  ret = stm32_hptc_setup();
	  if (ret < 0)
	    {
		  syslog(LOG_ERR, "ERROR: stm32_hptc_setup() failed: %d\n", ret);
	    }
	#endif



	#ifdef CONFIG_FS_PROCFS
	  /* Mount the procfs file system */

	  ret = mount(NULL, STM32_PROCFS_MOUNTPOINT, "procfs", 0, NULL);
	  if (ret < 0)
	    {
	      syslog(LOG_ERR, "ERROR: Failed to mount procfs at %s: %d\n",
	             STM32_PROCFS_MOUNTPOINT, ret);
	    }
	#endif

	#if !defined(CONFIG_ARCH_LEDS) && defined(CONFIG_USERLED_LOWER)
	  /* Register the LED driver */

	  ret = userled_lower_initialize(LED_DRIVER_PATH);
	  if (ret < 0)
	    {
	      syslog(LOG_ERR, "ERROR: userled_lower_initialize() failed: %d\n", ret);
	    }
	#endif


	#ifdef CONFIG_CAN
	  /* Initialize CAN and register the CAN driver. */

	  ret = stm32_can_setup();
	  if (ret < 0)
	    {
	      syslog(LOG_ERR, "ERROR: stm32_can_setup failed: %d\n", ret);
	    }
	#endif

	#ifdef CONFIG_MB_IMU_ADIS16XXX
	  /* Create a lis3dsh driver instance fitting the chip built into stm32f4discovery */

	  ret = stm32_adis16xxxinitialize("/dev/imu0");
	  if (ret < 0)
	    {
	      serr("ERROR: Failed to initialize ADIS16XXX driver: %d\n", ret);
	    }
	#endif


	#ifdef CONFIG_STM32F7_BBSRAM
	  /* Initialize battery-backed RAM */

	  (void)stm32_bbsram_int();
	#endif

	#if defined(CONFIG_FAT_DMAMEMORY)
	  if (stm32_dma_alloc_init() < 0)
	    {
	      syslog(LOG_ERR, "DMA alloc FAILED");
	    }
	#endif

	#if defined(CONFIG_NUCLEO_SPI_TEST)
	  /* Create SPI interfaces */

	  ret = stm32_spidev_bus_test();
	  if (ret != OK)
	    {
	      syslog(LOG_ERR, "ERROR: Failed to initialize SPI interfaces: %d\n", ret);
	      return ret;
	    }
	#endif
	#endif


#if defined(CONFIG_NSH_LIBRARY) && !defined(CONFIG_LIB_BOARDCTL)
  /* Perform NSH initialization here instead of from the NSH.  This
   * alternative NSH initialization is necessary when NSH is ran in user-space
   * but the initialization function must run in kernel space.
   */

  (void)board_app_initialize(0);
#endif
}
#endif
