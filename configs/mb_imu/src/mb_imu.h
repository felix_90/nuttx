/************************************************************************************
 * configs/mB_imu/src/mb_imu.h
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            Mark Olsson <post@markolsson.se>
 *            David Sidrane <david_s5@nscdg.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __CONFIGS_MB_IMU_SRC_MB_IMU_H
#define __CONFIGS_MB_IMU_SRC_MB_IMU_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <stdint.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Configuration ********************************************************************/

/* procfs File System */

#ifdef CONFIG_FS_PROCFS
#  ifdef CONFIG_NSH_PROC_MOUNTPOINT
#    define STM32_PROCFS_MOUNTPOINT CONFIG_NSH_PROC_MOUNTPOINT
#  else
#    define STM32_PROCFS_MOUNTPOINT "/proc"
#  endif
#endif

/* mB-imu GPIO Pin Definitions **************************************************/
/* LED
 *
 * The mB-imu board has numerous LEDs but only three, LD1 a Green LED, LD2 a
 * Blue LED and LD3 a Red LED, that can be controlled by software. The following definitions assume
 * the default Solder Bridges are installed.
 */

#define GPIO_LD1       (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | \
                        GPIO_PORTB | GPIO_PIN1)
#define GPIO_LD2       (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | \
                        GPIO_PORTB | GPIO_PIN2)

#define GPIO_LED_GREEN GPIO_LD1
#define GPIO_LED_RED  GPIO_LD2

#define LED_DRIVER_PATH "/dev/userleds"

/* CAN ***************************************************************************/
#define GPIO_CAN_RS (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET | \
                        GPIO_PORTD | GPIO_PIN9)

/* IMU
 *
 */
#define GPIO_MB_IMU_ADIS16XXX_POWER (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET | \
                        GPIO_PORTD | GPIO_PIN10)

#define MB_IMU_SPI_BUS4_CS0  \
  (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|GPIO_OUTPUT_SET|GPIO_PORTE|GPIO_PIN11)

#define GPIO_MB_IMU_ADIS16XXX_DATA_READY \
  (GPIO_INPUT|GPIO_FLOAT|GPIO_AF0|GPIO_SPEED_50MHz|GPIO_PORTD|GPIO_PIN11)


#define GPIO_SPI4_CS0 MB_IMU_SPI_BUS4_CS0
//#define GPIO_SPI_CS    (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_SET)

// #define GPIO_SPI1_CS0   (GPIO_SPI_CS | GPIO_PORTA | GPIO_PIN15)
// #define GPIO_SPI1_CS1   (GPIO_SPI_CS | GPIO_PORTC | GPIO_PIN15)
// #define GPIO_SPI1_CS2   (GPIO_SPI_CS | GPIO_PORTC | GPIO_PIN14)
// #define GPIO_SPI1_CS3   (GPIO_SPI_CS | GPIO_PORTC | GPIO_PIN2)
// #define GPIO_SPI2_CS0   (GPIO_SPI_CS | GPIO_PORTD | GPIO_PIN7)
// #define GPIO_SPI2_CS1   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN1)
// #define GPIO_SPI2_CS2   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN2)
// #define GPIO_SPI2_CS3   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN3)
// #define GPIO_SPI3_CS0   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN4)
// #define GPIO_SPI3_CS1   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN5)
// #define GPIO_SPI3_CS2   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN6)
// #define GPIO_SPI3_CS3   (GPIO_SPI_CS | GPIO_PORTG | GPIO_PIN7)

/* Logical SPI Chip Selects used to index */

// #define NUCLEO_SPI_BUS1_CS0  0
// #define NUCLEO_SPI_BUS1_CS1  1
// #define NUCLEO_SPI_BUS1_CS2  2
// #define NUCLEO_SPI_BUS1_CS3  3
// #define NUCLEO_SPI_BUS2_CS0  4
// #define NUCLEO_SPI_BUS2_CS1  5
// #define NUCLEO_SPI_BUS2_CS2  6
// #define NUCLEO_SPI_BUS2_CS3  7
// #define NUCLEO_SPI_BUS3_CS0  8
// #define NUCLEO_SPI_BUS3_CS1  9
// #define NUCLEO_SPI_BUS3_CS2  10
// #define NUCLEO_SPI_BUS3_CS3  11

/* USB OTG FS
 *
 * PA9  OTG_FS_VBUS VBUS sensing (also connected to the green LED)
 * PC0  OTG_FS_PowerSwitchOn
 * PD5  OTG_FS_Overcurrent
 */

#define GPIO_OTGHS_VBUS   (GPIO_INPUT|GPIO_FLOAT|GPIO_SPEED_100MHz|\
                           GPIO_OPENDRAIN|GPIO_PORTB|GPIO_PIN13)

// #define GPIO_OTGHS_PWRON  (GPIO_OUTPUT|GPIO_FLOAT|GPIO_SPEED_100MHz| GPIO_PUSHPULL|GPIO_PORTG|GPIO_PIN6)
// #ifdef CONFIG_USBHOST
// #  define GPIO_OTGHS_OVER (GPIO_INPUT|GPIO_EXTI|GPIO_FLOAT| GPIO_SPEED_100MHz|GPIO_PUSHPULL| GPIO_PORTG|GPIO_PIN7)
// #else
// #  define GPIO_OTGHS_OVER (GPIO_INPUT|GPIO_FLOAT|GPIO_SPEED_100MHz| GPIO_PUSHPULL|GPIO_PORTG|GPIO_PIN7)
// #endif

/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_spidev_initialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the mB-imu board.
 *
 ************************************************************************************/

#if defined(CONFIG_SPI)
void stm32_spidev_initialize(void);
#endif

/************************************************************************************
 * Name: stm32_spidev_bus_test
 *
 * Description:
 *   Called to create the defined SPI buses and test them by initializing them
 *   and sending the NUCLEO_SPI_TEST (no chip select).
 *
 ************************************************************************************/

#if defined(CONFIG_NUCLEO_SPI_TEST)
int stm32_spidev_bus_test(void);
#endif

/************************************************************************************
 * Name: stm32_dma_alloc_init
 *
 * Description:
 *   Called to create a FAT DMA allocator
 *
 * Returned Value:
 *   0 on success or -ENOMEM
 *
 ************************************************************************************/

void stm32_dma_alloc_init(void);

#if defined (CONFIG_FAT_DMAMEMORY)
int stm32_dma_alloc_init(void);
#endif

/****************************************************************************
 * Name: stm32_sdio_initialize
 *
 * Description:
 *   Called at application startup time to initialize the SCMMC functionality.
 *
 ****************************************************************************/

#ifdef CONFIG_MMCSD
int stm32_sdio_initialize(void);
#endif

/************************************************************************************
 * Name: stm32_usbinitialize
 *
 * Description:
 *   Called from stm32_usbinitialize very early in inialization to setup USB-related
 *   GPIO pins for the nucleo-144 board.
 *
 ************************************************************************************/

#ifdef CONFIG_STM32F7_OTGHS
void stm32_usbinitialize(void);
#endif

/************************************************************************************
 * Name: stm32_adc_setup
 *
 * Description:
 *   Initialize ADC and register the ADC driver.
 *
 ************************************************************************************/

#ifdef CONFIG_ADC
int stm32_adc_setup(void);
#endif


/****************************************************************************
 * Name: stm32_can_setup
 *
 * Description:
 *  Initialize CAN and register the CAN device
 *
 ****************************************************************************/

#ifdef CONFIG_CAN
int stm32_can_setup(void);
#endif


/****************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ****************************************************************************/

#ifdef CONFIG_HPTC
int stm32_hptc_setup(void);
#endif

/****************************************************************************
 * Name: stm32_adis16xxxinitialize
 *
 * Description:
 *   Called to configure SPI 4, and to register ADIS16XXX
 *   and its external interrupt
 *
 *
 ****************************************************************************/

#ifdef CONFIG_MB_IMU_ADIS16XXX
int stm32_adis16xxxinitialize(FAR const char *devpath);
#endif



/************************************************************************************
 * Name: stm32_bbsram_int
 ************************************************************************************/

#ifdef CONFIG_STM32F7_BBSRAM
int stm32_bbsram_int(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_MB_IMU_SRC_MB_IMU_H */
