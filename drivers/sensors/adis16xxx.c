/****************************************************************************
 * drivers/sensors/adis16xxx.c
 * Character driver for the Analog Devices ADIS16375 and ADIS16488 IMU.
 *
 *   Copyright (C) 2017 TU-Braunschweig, Institut für Flugführung. All rights reserved.
 *   Author: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>
#include <string.h>
#include <semaphore.h>

#include <nuttx/kmalloc.h>
#include <nuttx/wqueue.h>

#include <nuttx/fs/fs.h>
#include <nuttx/sensors/adis16xxx.h>
#include <nuttx/random.h>

#if defined(CONFIG_SPI) && defined(CONFIG_SENSORS_ADIS16XXX)

/****************************************************************************
 * Private
 ****************************************************************************/


struct adis16xxx_dev_s
{
  uint8_t              open_count;   /* Number of times the device has been opened */

  FAR struct adis16xxx_dev_s *flink;     /* Supports a singly linked list of
                                       * drivers */
  FAR struct spi_dev_s *spi;          /* Pointer to the SPI instance */
  FAR struct adis16xxx_config_s *config; /* Pointer to the configuration of the
                                       * aADIS16xxx sensor */
  uint16_t product_id;                /* detected device */
  int32_t delay_ns;                   /* data delay due to filtering */
  uint16_t exchange_buf_size;
  uint16_t tx_buf[ADIS16XXX_EXCHANGE_BUF_SIZE];
  uint16_t rx_buf[ADIS16XXX_EXCHANGE_BUF_SIZE];

  /* Semaphores */
  sem_t closesem;     /* Locks out new open while close is in progress */
  sem_t datasem;                      /* Manages exclusive access to this
                                       * structure */


  struct adis16488_msg_s data;   /* The data as measured by the sensor */
  struct work_s work;                 /* The work queue is responsible for
                                       * retrieving the data from the sensor
                                       * after the arrival of new data was
                                       * signalled in an interrupt */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static uint16_t adis16xxx_read_reg16(FAR struct adis16xxx_dev_s *dev,
                                 uint8_t const page,
								 uint8_t const reg_addr);
#if 0
static uint32_t adis16xxx_read_reg32(FAR struct adis16xxx_dev_s *dev,
                                 uint8_t const page,
								 uint8_t const reg_addr);
#endif
static void adis16xxx_write_reg16(FAR struct adis16xxx_dev_s *dev,
								  uint8_t const page,
                                  uint8_t const reg_addr,
                                  uint16_t const reg_data);
#if 0
static void adis16xxx_write_reg32(FAR struct adis16xxx_dev_s *dev,
								  uint8_t const page,
                                  uint8_t const reg_addr,
                                  uint32_t const reg_data);
#endif

static void adis16xxx_reset(FAR struct adis16xxx_dev_s *dev);
static void adis16xxx_read_measurement_data(FAR struct adis16xxx_dev_s *dev);
static int adis16xxx_interrupt_handler(int irq, FAR void *context, FAR void *arg);
static void adis16xxx_worker(FAR void *arg);

static int adis16xxx_open(FAR struct file *filep);
static int adis16xxx_close(FAR struct file *filep);
static ssize_t adis16xxx_read(FAR struct file *, FAR char *, size_t);
static ssize_t adis16xxx_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen);
static int adis16xxx_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_adis16xxx_fops =
{
  adis16xxx_open,
  adis16xxx_close,
  adis16xxx_read,
  adis16xxx_write,
  NULL,
  adis16xxx_ioctl
#ifndef CONFIG_DISABLE_POLL
  , NULL
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL
#endif
};

/* Single linked list to store instances of drivers */

static struct adis16xxx_dev_s *g_adis16xxx_list = NULL;

/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Name: adis16xxx_read_register
 ****************************************************************************/

static uint16_t adis16xxx_read_reg16(FAR struct adis16xxx_dev_s *dev,
                                 uint8_t const page,
								 uint8_t const addr)
{
  uint16_t data;
  /* Lock the SPI bus so that only one device can access it at the same time */

  SPI_LOCK(dev->spi, true);

  /* Set CS to low which selects the ADIS16XXX */

  SPI_SELECT(dev->spi, dev->config->spi_devid, true);

  /* set page for write*/
  SPI_SEND(dev->spi,0x8000 | (page & 0x00ff));

  /* wait at least 2us */
  up_udelay(2);

  /* send read address command */
  SPI_SEND(dev->spi,((addr<<8) & 0x7f00));

  /* wait at least 2us */
  up_udelay(2);

  /* send dummy to read data */
  data = (uint16_t) (SPI_SEND(dev->spi, 0x0000));

  /* wait at least 2us */
  up_udelay(2);

  /* Set CS to high which deselects the ADIS16XXX */

  SPI_SELECT(dev->spi, dev->config->spi_devid, false);

  /* Unlock the SPI bus */

  SPI_LOCK(dev->spi, false);

  return data;
}

/****************************************************************************
 * Name: adis16xxx_write_register
 ****************************************************************************/
#if 0
static uint32_t adis16xxx_read_reg32(FAR struct adis16xxx_dev_s *dev,
                                 uint8_t const page,
								 uint8_t const addr)
{
	  uint32_t data;
	  /* Lock the SPI bus so that only one device can access it at the same time */

	  SPI_LOCK(dev->spi, true);

	  /* Set CS to low which selects the ADIS16XXX */

	  SPI_SELECT(dev->spi, dev->config->spi_devid, true);

	  /* set page */
	  SPI_SEND(dev->spi,0x8000 | (page & 0x00ff));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* send read address command */
	  SPI_SEND(dev->spi,((addr<<8) & 0x7f00));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* send next addr and read first nibble of data */
	  data = (uint32_t) (SPI_SEND(dev->spi, (((addr+2)<<8) & 0x7f00)));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* send dummy to read last data */
	  data |= ( (uint32_t) (SPI_SEND(dev->spi, 0x0000)) ) << 16;

	  /* wait at least 2us */
	  up_udelay(2);

	  /* Set CS to high which deselects the ADIS16XXX */

	  SPI_SELECT(dev->spi, dev->config->spi_devid, false);

	  /* Unlock the SPI bus */

	  SPI_LOCK(dev->spi, false);

	  return data;
}
#endif

static void adis16xxx_write_reg16(FAR struct adis16xxx_dev_s *dev,
								  uint8_t const page,
                                  uint8_t const addr,
                                  uint16_t const data)
{
	  /* Lock the SPI bus so that only one device can access it at the same time */

	  SPI_LOCK(dev->spi, true);

	  /* Set CS to low which selects the ADIS16XXX */

	  SPI_SELECT(dev->spi, dev->config->spi_devid, true);

	  /* set page */
	  SPI_SEND(dev->spi,0x8000 | (page & 0x00ff));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* write address and command low byte */
	  SPI_SEND(dev->spi,( 0x8000 | ( (addr << 8 ) & 0x7f00) | ( data & 0xFF )));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* write address and command high byte */
	  SPI_SEND(dev->spi,( 0x8000 | ( ((addr+1) << 8 ) & 0x7f00) | ((data >> 8) & 0xFF )));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* Set CS to high which deselects the ADIS16XXX */

	  SPI_SELECT(dev->spi, dev->config->spi_devid, false);

	  /* Unlock the SPI bus */

	  SPI_LOCK(dev->spi, false);
}

#if 0
static void adis16xxx_write_reg32(FAR struct adis16xxx_dev_s *dev,
								  uint8_t const page,
                                  uint8_t const addr,
                                  uint32_t const data)
{
	  /* Lock the SPI bus so that only one device can access it at the same time */

	  SPI_LOCK(dev->spi, true);

	  /* Set CS to low which selects the ADIS16XXX */

	  SPI_SELECT(dev->spi, dev->config->spi_devid, true);

	  /* set page */
	  SPI_SEND(dev->spi,0x8000 | (page & 0x00ff));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* write address and first data byte */
	  SPI_SEND(dev->spi,( 0x8000 | ( (addr << 8 ) & 0x7f00) | ( data & 0xFF )));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* write address and second data byte */
	  SPI_SEND(dev->spi,( 0x8000 | ( ((addr+1) << 8 ) & 0x7f00) | ((data >> 8) & 0xFF )));

	  /* wait at least 2us */
	  up_udelay(2);

	  /* write address and third data byte */
	  SPI_SEND(dev->spi,( 0x8000 | ( ((addr+1) << 8 ) & 0x7f00) | ((data >> 16) & 0xFF )));

	  /* wait at least 2us */
	  up_udelay(2);
	  /* Set CS to high which deselects the ADIS16XXX */

	  /* write address and last data byte */
	  SPI_SEND(dev->spi,( 0x8000 | ( ((addr+1) << 8 ) & 0x7f00) | ((data >> 24) & 0xFF )));

	  /* wait at least 2us */
	  up_udelay(2);
	  SPI_SELECT(dev->spi, dev->config->spi_devid, false);

	  /* Unlock the SPI bus */

	  SPI_LOCK(dev->spi, false);
}
#endif
/****************************************************************************
 * Name: adis16xxx_reset
 ****************************************************************************/

static void adis16xxx_reset(FAR struct adis16xxx_dev_s *dev)
{
  adis16xxx_write_reg16(dev, 0x03, 0x02, 0x0080);

  //wait > 74 ms    for ADIS 16375
  //wait > 120ms    for ADIS 16488
  up_mdelay(150);
}

/****************************************************************************
 * Name: adis16xxx_read_measurement_data
 ****************************************************************************/

static void adis16xxx_read_measurement_data(FAR struct adis16xxx_dev_s *dev)
{
  int ret;


  /* Lock the SPI bus so that only one device can access it at the same time */

  SPI_LOCK(dev->spi, true);

  /* Set CS to low which selects the ADIS16XXX */

  SPI_SELECT(dev->spi, dev->config->spi_devid, true);



  /* spi transfer via SPI_EXCHANGE */
  SPI_EXCHANGE(dev->spi,dev->tx_buf,dev->rx_buf,dev->exchange_buf_size);

  /* Set CS to high which deselects the ADIS16XXX */

  SPI_SELECT(dev->spi, dev->config->spi_devid, false);

  /* Unlock the SPI bus */

  SPI_LOCK(dev->spi, false);

  /* Aquire the semaphore before the data is copied */

  ret = nxsem_wait(&dev->datasem);
  if (ret < 0)
    {
      snerr("ERROR: Could not aquire dev->datasem: %d\n", ret);
      DEBUGASSERT(ret == -EINTR);
      return;
    }
  /* Copy retrieve data to internal data structure */


  /*dev->data.sys_e_flag = dev->rx_buf[2];*/
  dev->data.sys_e_flag |= dev->rx_buf[2]; //FIXME: workaround for sticky error_flags

  dev->data.diag_sts   = dev->rx_buf[4];
  dev->data.alm_sts    = dev->rx_buf[6];               /* */
  dev->data.temp_out   = dev->rx_buf[8];                 /*  */
  dev->data.x_gyro   = (dev->rx_buf[12] << 16) | dev->rx_buf[10];                /* */
  dev->data.y_gyro   = (dev->rx_buf[16] << 16) | dev->rx_buf[14];                 /*  */
  dev->data.z_gyro   = (dev->rx_buf[20] << 16) | dev->rx_buf[18];               /* */
  dev->data.x_accl   = (dev->rx_buf[24] << 16) | dev->rx_buf[22];               /* */
  dev->data.y_accl   = (dev->rx_buf[28] << 16) | dev->rx_buf[26];                  /*  */
  dev->data.z_accl   = (dev->rx_buf[32] << 16) | dev->rx_buf[30];               /* */
  if(dev->product_id == 16488){
    dev->data.x_mag   = dev->rx_buf[34];               /* */
    dev->data.y_mag   = dev->rx_buf[36];                 /*  */
    dev->data.z_mag   = dev->rx_buf[38];               /* */
    dev->data.p_stat  = (dev->rx_buf[42] << 16) | dev->rx_buf[40];              /* */
  }

  /* Give back the semaphore */

  nxsem_post(&dev->datasem);

  /* Feed sensor data to entropy pool */

//  add_sensor_randomness((x_gyr << 16) ^ (y_gyr << 8) ^ z_gyr);
}

/****************************************************************************
 * Name: adis16xxx_interrupt_handler
 ****************************************************************************/

static int adis16xxx_interrupt_handler(int irq, FAR void *context, FAR void *arg)
{
  /* This function should be called upon a rising edge on the ADIS16XXX new data
   * interrupt pin since it signals that new data has been measured.
   */

  FAR struct adis16xxx_dev_s *priv = 0;
  int ret;

  /* Find out which ADIS16XXX device caused the interrupt */
  /* TODO:
   * As irq is not unique, use config as arg when attaching irq and get it back from *context).
   */
  for (priv = g_adis16xxx_list; priv && priv->config->irq != irq;
       priv = priv->flink);
  DEBUGASSERT(priv != NULL);

  /* Task the worker with retrieving the latest sensor data. We should not do
   * this in a interrupt since it might take too long. Also we cannot lock the
   * SPI bus from within an interrupt.
   */

  DEBUGASSERT(priv->work.worker == NULL);
  ret = work_queue(HPWORK, &priv->work, adis16xxx_worker, priv, 0);
  if (ret < 0)
    {
      snerr("ERROR: Failed to queue work: %d\n", ret);
      return ret;
    }

  return OK;
}

/****************************************************************************
 * Name: adis16xxx_worker
 ****************************************************************************/

static void adis16xxx_worker(FAR void *arg)
{
  FAR struct adis16xxx_dev_s *priv = (FAR struct adis16xxx_dev_s *)(arg);
  DEBUGASSERT(priv != NULL);

  /* Read out the latest sensor data */

  adis16xxx_read_measurement_data(priv);
}

/****************************************************************************
 * Name: adis16xxx_open
 ****************************************************************************/

static int adis16xxx_open(FAR struct file *filep)
{
  //TODO: move to IOCTL and leave open an empty function

  FAR struct inode *inode = filep->f_inode;
  FAR struct adis16xxx_dev_s *priv = inode->i_private;
#ifdef CONFIG_DEBUG_SENSORS_INFO
  uint16_t reg_content;
#endif
  int ret;
  uint8_t tmp;

  DEBUGASSERT(priv != NULL);


  sninfo("open() called\n");
  /* If the port is the middle of closing, wait until the close is finished.
     * If a signal is received while we are waiting, then return EINTR.
     */

    ret = nxsem_wait(&priv->closesem);
    if (ret < 0)
      {
        /* A signal received while waiting for the last close operation. */

        return ret;
      }

    tmp = priv->open_count + 1;
    if (tmp == 0)
      {
        /* More than 255 opens; uint8_t overflows to zero */

        ret = -EMFILE;
        goto errout_with_sem;
      }

  if (tmp == 1){
	  /* Perform a reset */
	  sninfo("reset imu\n");
	  adis16xxx_reset(priv);

	  /* identify IMU */
	  sninfo("read product id\n");
	  /* read PROD_ID: page 0x00, addr 0x7E */
	  priv->product_id = adis16xxx_read_reg16(priv,0x00,0x7E);
	  sninfo("product id read: %d\n",priv->product_id);

	  switch (priv->product_id)
	  {
	  case 16488:
		priv->exchange_buf_size=ADIS16488_EXCHANGE_BUF_SIZE;

		for(int i=0; i<priv->exchange_buf_size-2; i+=2){
			priv->tx_buf[i]=priv->tx_buf[i+1]=(i+0x08) << 8;
		}
		priv->tx_buf[priv->exchange_buf_size-2]=priv->tx_buf[priv->exchange_buf_size-1]=0x0000;

		/* set FNCTIO_CTRL: page 0x03, addr 0x06 */
		/* Sync clock input disabled, Data-ready positiv edge on DIO2 */
		adis16xxx_write_reg16(priv, 0x03,0x06,0x000C);

		/* set Filter Type */

		/* FilterA: 120 tap -3dB@310Hz,25ms delay  <=== USED <===*/
		/* FilterB: 120 tap -3dB@55Hz, 25ms delay*/
		/* FilterC:  32 tap -3dB@275Hz, 6ms delay*/
		/* FilterD:  32 tap -3dB@63Hz,  6ms delay*/

		/* set FILTR_BNK_0: page 0x03, addr 0x16 */
		adis16xxx_write_reg16(priv, 0x03,0x16,0x4924);

		/* set FILTR_BNK_1: page 0x03, addr 0x18 (additional 16488 bits are don't care on 16375) */
		adis16xxx_write_reg16(priv, 0x03,0x18,0x0924);

		priv->delay_ns = 25000000;


	#ifdef CONFIG_DEBUG_SENSORS_INFO
	  /* Read back the content of all control registers for debug purposes */

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x06);
	  sninfo("ADIS16488_SEQ_CNT = %04x\n", reg_content);

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x08);
	  sninfo("ADIS16488_SYS_E_FLAG = %04x\n", reg_content);

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x0A);
	  sninfo("ADIS16488_DIAG_STS = %04x\n", reg_content);

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x0C);
	  sninfo("ADIS16488_ALM_STS = %04x\n", reg_content);
	#endif


		//set page 0x00 (write)
		adis16xxx_read_reg16(priv,0x00,0x00);

		break;





	  case 16375:
		  priv->exchange_buf_size=ADIS16375_EXCHANGE_BUF_SIZE;

			for(int i=0; i<priv->exchange_buf_size-2; i+=2){
				priv->tx_buf[i]=priv->tx_buf[i+1]=(i+0x08) << 8;
			}
			priv->tx_buf[priv->exchange_buf_size-2]=priv->tx_buf[priv->exchange_buf_size-1]=0x0000;


		/* set FNCTIO_CTRL: page 0x03, addr 0x06 */
		/* Sync clock input disabled, Data-ready positiv edge on DIO2 */
		adis16xxx_write_reg16(priv, 0x03,0x06,0x000C);

		/* set Filter Type */

		/* FilterA: 120 tap -3dB@310Hz,25ms delay  <=== USED <===*/
		/* FilterB: 120 tap -3dB@55Hz, 25ms delay*/
		/* FilterC:  32 tap -3dB@275Hz, 6ms delay*/
		/* FilterD:  32 tap -3dB@63Hz,  6ms delay*/

		/* set FILTR_BNK_0: page 0x03, addr 0x16 */
		adis16xxx_write_reg16(priv, 0x03,0x16,0x4924);

		/* set FILTR_BNK_1: page 0x03, addr 0x18 (additional 16488 bits are don't care on 16375) */
		adis16xxx_write_reg16(priv, 0x03,0x18,0x0924);

		priv->delay_ns = 25000000;


	#ifdef CONFIG_DEBUG_SENSORS_INFO
	  /* Read back the content of all control registers for debug purposes */

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x06);
	  sninfo("ADIS16375_SEQ_CNT = %04x\n", reg_content);

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x08);
	  sninfo("ADIS16375_SYS_E_FLAG = %04x\n", reg_content);

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x0A);
	  sninfo("ADIS16375_DIAG_STS = %04x\n", reg_content);

	  reg_content = adis16xxx_read_reg16(priv, 0x00, 0x0C);
	  sninfo("ADIS16375_ALM_STS = %04x\n", reg_content);
	#endif


		//set page 0x00 (write)
		adis16xxx_read_reg16(priv,0x00,0x00);

		break;

	  /* no known device detected */
	  default:
		  return -ENODEV;



	  }

	  sninfo("Attach interrupt handler\n");
	  priv->config->attach(priv->config, &adis16xxx_interrupt_handler);

  }
  /* Save the new open count on success */

  priv->open_count = tmp;

errout_with_sem:
  nxsem_post(&priv->closesem);

  return ret;
}

/****************************************************************************
 * Name: adis16xxx_close
 ****************************************************************************/

static int adis16xxx_close(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct adis16xxx_dev_s *priv = inode->i_private;
  int ret;

  DEBUGASSERT(priv != NULL);

  sninfo("close() called\n");
  do
  {
	  ret = nxsem_wait(&priv->closesem);
  }
  while (ret ==-EINTR);

  //(void)adis16xxx_takesem(&priv->closesem, false);



    if (priv->open_count > 1)
      {
        priv->open_count--;
        nxsem_post(&priv->closesem);
        return OK;
      }

    /* There are no more references to the port */

    priv->open_count = 0;

  sninfo("Detach interrupt handler\n");
  priv->config->attach(priv->config, NULL);
  /* Perform a reset */

//  adis16xxx_reset(priv);

  nxsem_post(&priv->closesem);
  return OK;
}

/****************************************************************************
 * Name: adis16xxx_read
 ****************************************************************************/

static ssize_t adis16xxx_read(FAR struct file *filep, FAR char *buffer,
                           size_t buflen)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct adis16xxx_dev_s *priv = inode->i_private;
  FAR struct adis16488_msg_s *data;
  int ret;

  DEBUGASSERT(priv != NULL);

  /* Check if enough memory was provided for the read call */

  if (buflen < sizeof(FAR struct adis16488_msg_s))
    {
      snerr("ERROR: Not enough memory for reading out a sensor data sample\n");
      return -ENOSYS;
    }

  /* Aquire the semaphore before the data is copied */

  ret = nxsem_wait(&priv->datasem);
  if (ret < 0)
    {
      snerr("ERROR: Could not aquire priv->datasem: %d\n", ret);
      DEBUGASSERT(ret == -EINTR);
      return ret;
    }

  /* Copy the sensor data into the buffer */

  data = (FAR struct adis16488_msg_s *)buffer;
  //memset(data, 0, sizeof(FAR struct adis16488_msg_s));

  *data = priv->data;

  //FIXME: workaround to catch error_flags
  priv->data.sys_e_flag=0; /*clear error flags*/


  /* Give back the semaphore */

  nxsem_post(&priv->datasem);

  return sizeof(FAR struct adis16488_msg_s);
}

/****************************************************************************
 * Name: adis16xxx_write
 ****************************************************************************/

static ssize_t adis16xxx_write(FAR struct file *filep, FAR const char *buffer,
                            size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: adis16xxx_ioctl
 ****************************************************************************/

static int adis16xxx_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  int ret = OK;

  switch (cmd)
    {
      /* Command was not recognized */

    default:
      snerr("ERROR: Unrecognized cmd: %d\n", cmd);
      ret = -ENOTTY;
      break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: adis16xxx_register
 *
 * Description:
 *   Register the ADIS16XXX character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/imu0"
 *   spi     - An instance of the SPI interface to use to communicate with
 *             ADIS16XXX
 *   config  - configuration for the ADIS16XXX driver. For details see
 *             description above.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int adis16xxx_register(FAR const char *devpath, FAR struct spi_dev_s *spi,
                    FAR struct adis16xxx_config_s *config)
{
  FAR struct adis16xxx_dev_s *priv;
  int ret;

  /* Sanity check */

  DEBUGASSERT(spi != NULL);
  DEBUGASSERT(config != NULL);

  /* Initialize the ADIS16XXX device structure */

  priv = (FAR struct adis16xxx_dev_s *)kmm_malloc(sizeof(struct adis16xxx_dev_s));
  if (priv == NULL)
    {
      snerr("ERROR: Failed to allocate instance\n");
      return -ENOMEM;
    }
 sninfo("memory for adis16xxx_dev_s *priv allocated\n");
  priv->open_count = 0;
  priv->spi         = spi;
  priv->config      = config;
  priv->work.worker = NULL;


//TODO:  memset((void*)(&priv->data), 0, sizeof( struct adis16488_msg_s));

  /* Initialize sensor data access semaphore */
  sninfo("init semaphore priv->datasem\n");
  ret=nxsem_init(&priv->datasem, 0, 1);
  if (ret < 0)
    {
      snerr("ERROR: nx_sem_init failed\n");
      kmm_free(priv);
      return ret;
    }

  ret=nxsem_init(&priv->closesem,0,1);
  if (ret < 0)
    {
      snerr("ERROR: nx_sem_init failed\n");
      nxsem_destroy(&priv->datasem);
      kmm_free(priv);
      return ret;
    }



  /* Setup SPI frequency and mode */

  SPI_SETFREQUENCY(spi, ADIS16XXX_SPI_FREQUENCY);
  SPI_SETBITS(spi, ADIS16XXX_SPI_BITS);
  SPI_SETMODE(spi, ADIS16XXX_SPI_MODE);

#if 0
  /* Attach the interrupt handler */
  /*TODO: use priv instead of priv->config */
  sninfo("Attach interrupt handler\n");
  ret = priv->config->attach(priv->config, &adis16xxx_interrupt_handler);
  if (ret < 0)
    {
      snerr("ERROR: Failed to attach interrupt\n");
      kmm_free(priv);
      nxsem_destroy(&priv->datasem);
      nxsem_destroy(&priv->closesem);
      return ret;
    }
#endif
  /* Register the character driver */
  sninfo("register driver\n");
  ret = register_driver(devpath, &g_adis16xxx_fops, 0666, priv);
  if (ret < 0)
    {
      snerr("ERROR: Failed to register driver: %d\n", ret);
      kmm_free(priv);
      nxsem_destroy(&priv->datasem);
      nxsem_destroy(&priv->closesem);
      return ret;
    }

  /* Since we support multiple ADIS16XXX devices, we will need to add this new
   * instance to a list of device instances so that it can be found by the
   * interrupt handler based on the received IRQ number. */
  sninfo("update list of device instances\n");
  priv->flink = g_adis16xxx_list;
  g_adis16xxx_list = priv;

  return OK;
}

#endif /* CONFIG_SPI && CONFIG_SENSORS_ADIS16XXX */
