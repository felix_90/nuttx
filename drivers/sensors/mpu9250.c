/****************************************************************************
 * drivers/sensors/mpu9250.c
 * Character driver for the MPU9250.c
 ****************************************************************************/


/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <fixedmath.h>
#include <errno.h>
#include <debug.h>

#include <stdio.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/spi/spi.h>
#include <nuttx/sensors/mpu9250.h>

#ifdef CONFIG_SENSORS_MPU9250

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private
 ****************************************************************************/

struct mpu9250_dev_s
{
  FAR struct spi_dev_s *spi;	/* SPI struct vor SPI Communication*/
  FAR int devicid;				/* deviced ID for SPI_SELECT*/
  sem_t exclsem;           		/* exclusion *//* Saved SPI driver instance */
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     mpu9250_open(FAR struct file *filep);
static int     mpu9250_close(FAR struct file *filep);
static ssize_t mpu9250_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen);
static ssize_t mpu9250_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int     mpu9250_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_mpu9250fops =
{
  mpu9250_open,
  mpu9250_close,
  mpu9250_read,
  mpu9250_write,
  NULL,
  mpu9250_ioctl,    /* ioctl */
};

/****************************************************************************
 * Private Functionsd
 ****************************************************************************/

/****************************************************************************
 * Name: mpu9250_open
 *
 * Description:
 *   This function is called whenever the bma456 device is opened.
 *
 ****************************************************************************/

static int mpu9250_open(FAR struct file *filep)
{
	FAR struct inode *inode = filep->f_inode;
	FAR struct mpu9250_dev_s *priv;

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;
	priv = (FAR struct mpu9250_dev_s*)inode->i_private;
	DEBUGASSERT(priv);

	sem_post(&priv->exclsem);
	SPI_LOCK(priv->spi, false);
	SPI_LOCK(priv->spi, true);
	SPI_SETFREQUENCY(priv->spi, 10000);
	SPI_SETMODE(priv->spi,SPIDEV_MODE1);
	SPI_SETBITS(priv->spi,8);
	SPI_LOCK(priv->spi, false);
	return OK;
}

/****************************************************************************
 * Name: mpu9250_close
 *
 * Description:
 *   This routine is called when the bma456 device is closed.
 *
 ****************************************************************************/

static int mpu9250_close(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: mpu9250_read
 ****************************************************************************/

static ssize_t mpu9250_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{





	return OK;
}

/****************************************************************************
 * Name: mpu9250_write
 *
 * based on spi_transfer
 * Input Parameters:
 *   filep - file descriptor
 *   buffer -pointer to Byte Array, which contains all data to send
 *   size - size form 2 to 3 Byte
 *
 *
 ****************************************************************************/

static ssize_t mpu9250_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	return 0;
}

/****************************************************************************
 * Name: mpu9250_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/


static int mpu9250_ioctl (FAR struct file *filep, int cmd, unsigned long arg)
{
	FAR struct inode *inode = filep->f_inode;
	FAR struct mpu9250_dev_s *priv;
	int ret;



	char *buffer_pointer;



	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;
	priv = (FAR struct mpu9250_dev_s*)inode->i_private;
	DEBUGASSERT(priv);


	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
	}

	buffer_pointer = (char *)arg;



	/*get the Adress of the target reg and write a 1 (for read mode) at the beginning */
	//printf("Buffer_Pointer [0]: %x \n",buffer_pointer[0]);
	//printf("Buffer_Pointer [1]: %x \n",buffer_pointer[1]);
	//printf("Buffer_Pointer [2]: %x \n",buffer_pointer[2]);

	buffer_pointer[0] = 0x80 | buffer_pointer[0];
	printf("Buffer_Pointer [0] mit 1: %x \n",buffer_pointer[0]);

	sem_post(&priv->exclsem);
	SPI_LOCK(priv->spi, false);
	SPI_SELECT(priv->spi, priv->devicid, true);
	SPI_SNDBLOCK(priv->spi,buffer_pointer,1);
	SPI_RECVBLOCK(priv->spi,buffer_pointer,2);
	SPI_SELECT(priv->spi, priv->devicid, false);
	SPI_LOCK(priv->spi, true);

	/*first Byte is dummy byte - write zero*/
	buffer_pointer[0] = 0;




	return 0;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mpu9250_register
 *
 * Description:
 *   Register the bma456 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi - An instance of the SPI interface to use to communicate wit
 *   bus - the bus adress of the connected SPI Device
 *
 *
 * Returned Value:
 *   Zero (OK) on success;
 *
 ****************************************************************************/

int mpu9250_register(FAR const char *devpath,FAR struct spi_dev_s *spi, int num_dev)
{
	FAR struct mpu9250_dev_s *priv;
	int ret = 0;

	priv = (FAR struct mpu9250_dev_s *)kmm_zalloc(sizeof(struct mpu9250_dev_s));

	/*check*/
	priv->spi = spi;
	priv->exclsem.semcount = 1;
	//priv->devicid = SPIDEVTYPE_BMA456; //num_dev

	ret = register_driver(devpath, &g_mpu9250fops, 0666, priv);

	return ret;
}
#endif /* CONFIG_SPI && CONFIG_AD5662 */
