/****************************************************************************
 * include/nuttx/sensor/mpu9250.h

 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_SENSOR_MPU9250_H
#define __INCLUDE_NUTTX_SENSOR_MPU9250_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/irq.h>

#ifdef CONFIG_SENSORS_MPU9250

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/


/****************************************************************************
 * Public Types
 ****************************************************************************/

struct spi_dev_s;

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Name: mpu9250_register
 *
 * Description:
 *  This function will register the max31855 driver as /dev/tempN
 *  where N is the minor device number
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi     - An instance of the SPI interface to use to communicate with
 *             mpu9250
 *	 num_dev - number of device
 *
 * Returned Value:
 *   Zero is returned on success.  Otherwise, a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int mpu9250_register(FAR const char *devpath,FAR struct spi_dev_s *spi, int num_dev);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_AD7190 */
#endif /* __INCLUDE_NUTTX_analog_AD7190_H */
