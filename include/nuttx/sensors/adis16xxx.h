/********************************************************************************************
 * include/nuttx/sensors/adis16xxx.h
 *
 *   Copyright (C) 2016 Institut für Flugführung, TU-Braunschweig. All rights reserved.
 *   Author: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ********************************************************************************************/

#ifndef __INCLUDE_NUTTX_SENSORS_ADIS16XXX_H
#define __INCLUDE_NUTTX_SENSORS_ADIS16XXX_H

/********************************************************************************************
 * Included Files
 ********************************************************************************************/

#include <nuttx/irq.h>
#include <nuttx/config.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/spi/spi.h>

#if defined(CONFIG_SPI) && defined(CONFIG_SENSORS_ADIS16XXX)

/********************************************************************************************
 * Pre-processor Definitions
 ********************************************************************************************/

/* ADIS16XXX Register Definitions **************************************************************/

/* Control Register Definitions *************************************************************/



/* SPI BUS PARAMETERS ***********************************************************************/

#define ADIS16XXX_SPI_FREQUENCY  (8000000)        /* 8 MHz */
#define ADIS16XXX_SPI_BITS       (16)             /* 16 bit */
#define ADIS16XXX_SPI_MODE       (SPIDEV_MODE3)   /* Device uses SPI Mode 3: CPOL=1, CPHA=1 */
#define ADIS16XXX_EXCHANGE_BUF_SIZE (44)
#define ADIS16488_EXCHANGE_BUF_SIZE (44)
#define ADIS16375_EXCHANGE_BUF_SIZE (34)

/********************************************************************************************
 * Public Types
 ********************************************************************************************/

/* A reference to a structure of this type must be passed to the ADIS16XXX
 * driver. This structure provides information about the configuration
 * of the sensor and provides some board-specific hooks.
 *
 * Memory for this structure is provided by the caller.  It is not copied
 * by the driver and is presumed to persist while the driver is active.
 */

struct adis16xxx_config_s
{
  /* Since multiple ADIS16XXX can be connected to the same SPI bus we need
   * to use multiple spi device ids which are employed by NuttX to select/
   * deselect the desired ADIS16XXX chip via their chip select inputs.
   */
  int spi_devid;
  /* The IRQ number must be provided for each ADIS16XXX device so that
   * their interrupts can be distinguished.
   */
  int irq;
  /* Attach the ADIS16XXX interrupt handler to the GPIO interrupt of the
   * concrete ADIS16XXX instance.
   */
  int (*attach)(FAR struct adis16xxx_config_s *, xcpt_t);
};




/* This describes on IMU message */

begin_packed_struct struct adis16488_msg_s
{
  int32_t      timestamp_s;
  int32_t      timestamp_ns;
  uint16_t      sys_e_flag;               /*  */
  uint16_t      diag_sts;                 /* */
  uint16_t      alm_sts;               /* */
  int16_t      temp_out;                 /*  */
  int32_t      x_gyro;               /* */
  int32_t      y_gyro;                 /*  */
  int32_t      z_gyro;               /* */
  int32_t      x_accl;               /* */
  int32_t      y_accl;                 /*  */
  int32_t      z_accl;               /* */
  int16_t      x_mag;               /* */
  int16_t      y_mag;                 /*  */
  int16_t      z_mag;               /* */
  int32_t      p_stat;              /* */
} end_packed_struct;

begin_packed_struct struct adis16375_msg_s
{
  int32_t      timestamp_s;
  int32_t      timestamp_ns;
  uint16_t      sys_e_flag;               /*  */
  uint16_t      diag_sts;                 /* */
  uint16_t      alm_sts;               /* */
  int16_t      temp_out;                 /*  */
  int32_t      x_gyro;               /* */
  int32_t      y_gyro;                 /*  */
  int32_t      z_gyro;               /* */
  int32_t      x_accl;               /* */
  int32_t      y_accl;                 /*  */
  int32_t      z_accl;               /* */
} end_packed_struct;



/********************************************************************************************
 * Public Function Prototypes
 ********************************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/********************************************************************************************
 * Name: adis16xxx_register
 *
 * Description:
 *   Register the ADIS16XXX character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/adis0"
 *   spi - An instance of the SPI interface to use to communicate with ADIS16XXX
 *   config - configuration for the ADIS16XXX driver. For details see description above.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ********************************************************************************************/

int adis16xxx_register(FAR const char *devpath, FAR struct spi_dev_s *spi,
                    FAR struct adis16xxx_config_s *config);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_SPI && CONFIG_SENSORS_ADIS16XXX */
#endif /* __INCLUDE_NUTTX_SENSORS_ADIS16XXX_H */
